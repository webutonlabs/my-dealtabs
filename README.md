System requirements:
NodeJS v.8.10.0 +
NPM 6.14.5 +

How to run locally:
- copy .env.dist to .env.local and provide api host and api uri prefix
- npm install
- npm start

To create build:
- npm run build --production