import {toast} from 'react-toastify';
import {MdError, FaThumbsUp, FaInfo} from 'react-icons/all';
import React from 'react';

const errorToast = (content, autoClose = 1500) => {
    const options = {
        position: 'bottom-right',
        autoClose: autoClose,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    };

    const body = (
        <>
            <MdError/>
            <span style={{'margin-left': '5px', 'font-size': '16px', 'font-weight': '500'}}>{content}</span>
        </>
    );

    toast.error(body, options);
};

const successToast = (content, autoClose = 1500) => {
    const options = {
        position: 'bottom-right',
        autoClose: autoClose,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    };

    const body = (
        <>
            <FaThumbsUp/>
            <span style={{'margin-left': '5px', 'font-size': '16px', 'font-weight': '500'}}>{content}</span>
        </>
    );

    toast.success(body, options);
};

const infoToast = (content, autoClose = 4000) => {
    const options = {
        position: 'bottom-right',
        autoClose: autoClose,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    };

    const body = (
        <>
            <FaInfo/>
            <span style={{'margin-left': '5px', 'font-size': '16px', 'font-weight': '500'}}>{content}</span>
        </>
    );

    toast.info(body, options);
};

const toastFromQuery = () => {
    const urlSearchParams = new URLSearchParams(window.location.search);

    if (urlSearchParams.has('toast') && urlSearchParams.get('toast') === 'success') {
        successToast(urlSearchParams.get('message'));
    }

    if (urlSearchParams.has('toast') && urlSearchParams.get('toast') === 'error') {
        errorToast(urlSearchParams.get('message'), 3000);
    }
}

export {
    errorToast,
    successToast,
    toastFromQuery,
    infoToast
};