import {TYPE_BASIC, TYPE_PREMIUM} from "./Membership";

export class MembershipRestrictions {
    constructor(memberships) {
        this._data = memberships;
        this._membershipData = {
            type: 'basic',
            restrictions: {
                session_limit: 3,
                device_sync: false,
                windows_limit: 1,
                tabs_limit: 15,
                save_shared_session: false,
                quick_tabs_limit: 15,
                live_sync: false,
                collaboration: false
            } // initial state
        };
    }

    forUser(user) {
        switch (user.membership.membershipType) {
            case 'basic': {
                this.forBasic();

                break;
            }
            case 'premium': {
                this.forPremium();

                break;
            }
        }

        return this;
    }

    forBasic() {
        this._setMembershipData(TYPE_BASIC);

        return this;
    }

    forPremium() {
        this._setMembershipData(TYPE_PREMIUM);

        return this;
    }

    sessionLimit() {
        return this._membershipData.restrictions['session_limit'];
    }

    windowsLimit() {
        return this._membershipData.restrictions['windows_limit'];
    }

    tabsLimit() {
        return this._membershipData.restrictions['tabs_limit'];
    }

    deviceSync() {
        return this._membershipData.restrictions['device_sync'];
    }

    saveSharedSession() {
        return this._membershipData.restrictions['save_shared_session'];
    }

    quickTabsLimit() {
        return this._membershipData.restrictions['quick_tabs_limit'];
    }

    collaboration() {
        return this._membershipData.restrictions['collaboration'];
    }

    liveSync() {
        return this._membershipData.restrictions['live_sync'];
    }

    _setMembershipData(membershipType) {
        this._membershipData = this._data.find(membership => membership.type === membershipType);
    }
}