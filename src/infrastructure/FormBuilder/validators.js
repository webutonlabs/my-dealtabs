class Validator {
    constructor(message = '') {
        this.message = message;
    }

    getMessage = () => {
        return this.message;
    }
}

class Length extends Validator {
    constructor(min, message) {
        super(message);

        this.min = Number(min);
    }

    validate = (val) => {
        return val.length > this.min;
    }
}

class NotBlank extends Validator {
    constructor(message = 'Cannot be empty') {
        super(message);
    }

    validate = (val) => {
        return !(val === '' || val === undefined);
    }
}


class Email extends Validator {
    constructor(message = 'Email is invalid') {
        super(message);
    }

    validate = (val) => {
        const regExp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

        return regExp.test(String(val).toLowerCase());
    }
}

export {
    NotBlank,
    Email,
    Length
}