import React from "react";
import FormInput from "../../elements/Form/FormInput";
import FormManager from "./FormManager";
import Form from "../../elements/Form/Form";
import FormRow from "../../elements/Form/FormRow";
import FormGroup from "../../elements/Form/FormGroup";
import FormSelect from "../../elements/Form/FormSelect";

class FormBuilder extends React.Component {
    constructor(props) {
        super(props);
        this.state = {form: props.form}
    }

    handleChange = (e) => {
        let formManager = new FormManager(this.state.form);
        let target = e.target;
        let fieldName = e.target.name;

        formManager
            .changeFieldValue(fieldName, target.value)
            .validateField(fieldName)
            .validateForm();

        if (formManager.getChangeCallback(fieldName) !== null) {
            formManager.getChangeCallback(fieldName)(e);
        }

        this.setState({form: formManager.getForm()});
    }

    getChildrenElements = () => {
        let children = [];

        for (let [fieldName, field] of Object.entries(this.state.form.children)) {
            if (field.type === 'text' || field.type === 'password') {
                children.push(
                    <FormInput
                        key={`form-input-${fieldName}`}
                        onChange={this.handleChange}
                        field={field}
                        name={fieldName}
                    />
                )
            }

            if (field.type === 'select') {
                children.push(
                    <FormSelect
                        key={`form-select-${fieldName}`}
                        onChange={this.handleChange}
                        field={field}
                        name={fieldName}
                    />
                )
            }
        }

        return children;
    }

    render() {
        return (
            <Form form={this.state.form} onSave={this.props.onSave} onCancel={this.props.onCancel}>
                <FormRow>
                    <FormGroup sizeClass="col-12">
                        {this.getChildrenElements()}
                    </FormGroup>
                </FormRow>
            </Form>
        );
    }
}

export default FormBuilder;