class FormManager {
    constructor(form) {
        this.form = form;
    }

    getForm = () => {
        return this.form;
    }

    validateForm = () => {
        let errors = [];

        for (let [fieldName, field] of Object.entries(this.form.children)) {
            if (field['isValid'] === false) {
                errors.push(fieldName);
            }
        }

        this.form.isValid = errors.length === 0;

        return this;
    }

    validateField = (fieldName) => {
        let field = this.getFieldByName(fieldName);
        let errors = [];

        if (field.validators === undefined || field.validators.length === 0) {
            this.makeFieldUntouched(fieldName);

            return this;
        }

        field.validators.forEach(validator => {
            if (!validator.validate(field.value, this)) {
                errors.push(validator.getMessage());
            }
        })

        errors.length > 0 ? this.makeFieldInvalid(fieldName, errors[0]) : this.makeFieldValid(fieldName);

        return this;
    }

    getFieldByName = (fieldName) => {
        return this.form.children[fieldName];
    }

    setFieldByName = (fieldName, modifiedField) => {
        this.form.children[fieldName] = modifiedField;

        return this;
    }

    changeFieldValue = (fieldName, newValue) => {
        let field = this.getFieldByName(fieldName);
        field.value = newValue;
        this.setFieldByName(fieldName, field);

        return this;
    }

    makeFieldValid = (fieldName) => {
        let field = this.getFieldByName(fieldName);

        field.isValid = true;
        field.invalidMessage = '';
        field.touched = true;
        this.setFieldByName(fieldName, field)

        return this;
    }

    makeFieldInvalid = (fieldName, message = '') => {
        let field = this.getFieldByName(fieldName);

        field.isValid = false;
        field.invalidMessage = message;
        field.touched = true;
        this.setFieldByName(fieldName, field)

        return this;
    }

    makeFieldUntouched = (fieldName) => {
        let field = this.getFieldByName(fieldName);

        field.isValid = true;
        field.invalidMessage = '';
        field.touched = false;
        this.setFieldByName(fieldName, field)

        return this;
    }

    getChangeCallback = (fieldName) => {
        let field = this.getFieldByName(fieldName);

        if (field.changeCallback !== undefined) {
            return field.changeCallback;
        }

        return null;
    }
}

export default FormManager;