function sliceString(string, length) {
    if (length === undefined) {
        length = 55;
    }

    if (string.length > length) {
        string = string.slice(0, length);

        return `${string}...`;
    }

    return string;
}

function arraysMatch(arr1, arr2) {
    let areSame = true;
    arr1.forEach((item, index) => {
        if (arr2[index] !== item) {
            areSame = false;
        }
    })

    return areSame;
}

function makeArrayOfIds(entities) {
    let arr = [];
    entities.forEach(item => {
        arr.push(Number(item.id));
    })

    return arr;
}

function filterByEnableStatus(array) {
    let enabled = [];
    let disabled = [];

    array.forEach(item => {
        item['is_enabled'] ?  enabled.push(item) : disabled.push(item);
    })

    return [enabled, disabled];
}

const isUserPremium = (user) => {
    return null === user.membership.subscriptionExpires
        ? false
        : (new Date(user.membership.subscriptionExpires).getTime() > new Date().getTime());
}

export {
    sliceString,
    arraysMatch,
    makeArrayOfIds,
    filterByEnableStatus,
    isUserPremium
}