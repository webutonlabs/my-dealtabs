import React from "react";

class FormRow extends React.Component {
    render() {
        return (<div className="form-row">{this.props.children}</div>)
    }
}

export default FormRow;