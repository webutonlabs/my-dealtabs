import React from "react";
import {MdClear, MdCreate} from "react-icons/md";
import {AppContext} from "../../components/App/AppContext";

class Form extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        return (
            <>
                {this.props.name && <strong className="text-muted d-block mb-2">{this.props.name}</strong>}
                <form onSubmit={this.props.onSave}>
                    {this.props.children}
                    <div className="btn-group btn-group-sm">
                        <button disabled={!this.props.form.isValid} type="submit" className="btn btn-white">
                              <span className="text-light">
                                <MdCreate/>
                              </span> {t('Save')}
                        </button>
                        {this.props.onCancel &&
                            <button onClick={this.props.onCancel} type="button" className="btn btn-white">
                                  <span className="text-danger">
                                    <MdClear/>
                                  </span> {t('Cancel')}
                            </button>
                        }
                    </div>
                </form>
            </>
        )
    }
}

export default Form;