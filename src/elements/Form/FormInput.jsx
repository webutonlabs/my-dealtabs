import React from "react";
import {AppContext} from "../../components/App/AppContext";

class FormInput extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        const field = this.props.field;
        const type = field.type;

        let validClasses = '';
        if (field.touched && field.isValid) {
            validClasses = 'is-valid';
        }

        if (field.touched && !field.isValid) {
            validClasses = 'is-invalid';
        }

        const classes = `form-control ${validClasses}`;

        return (field.isValid ?
                <>
                    <strong className="text-muted d-block mb-2 mt-10">{t(field.label)}</strong>
                    <input autoComplete="off" name={this.props.name} onChange={this.props.onChange} type={type} className={classes}
                           placeholder={t(field.label)} value={field.value} />
                </> :
                <>
                    <strong className="text-muted d-block mb-2 mt-10">{t(field.label)}</strong>
                    <input autoComplete="off" name={this.props.name} onChange={this.props.onChange} type={type} className={classes}
                           placeholder={t(field.label)} value={field.value} />
                        <div className="invalid-feedback">{t(field.invalidMessage)}</div>
                </>
        )
    }
}

export default FormInput;