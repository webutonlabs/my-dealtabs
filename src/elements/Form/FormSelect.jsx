import React from "react";
import {AppContext} from "../../components/App/AppContext";

class FormSelect extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        let field = this.props.field;

        return (
            <>
                <strong className="text-muted d-block mb-2 mt-10">{t(field.label)}</strong>
                <select name={this.props.name} value={field.value} onChange={this.props.onChange} className="custom-select">
                    {field.data.map((item, index) => (<option key={item.id} value={item.id}>
                        {item.name === null ? `${t('Unnamed')} #${index + 1}` : (field.translate ? t(item.name) : item.name)}
                    </option>))}
                </select>
            </>
        );
    }
}

export default FormSelect;