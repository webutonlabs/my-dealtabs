import React from "react";

class FormGroup extends React.Component{
    render() {
        const classNames = `form-group ${this.props.sizeClass}`;

        return (<div className={classNames}>{this.props.children}</div>);
    }
}

export default FormGroup;