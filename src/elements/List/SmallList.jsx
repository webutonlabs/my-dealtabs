import React from "react";

class SmallList extends React.Component {
    render() {
        return (
            <ul className="list-group list-group-small list-group-flush fw-500">
                {this.props.children}
            </ul>
        )
    }
}

export default SmallList;