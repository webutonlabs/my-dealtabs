import React from "react";

class SmallListElement extends React.Component {
    render() {
        return (
            <li className="list-group-item d-flex px-3">
                <span className="text-semibold text-fiord-blue">{this.props.name}</span>
                <span className="ml-auto text-right text-semibold text-reagent-gray">{this.props.value}</span>
            </li>
        )
    }
}

export default SmallListElement;