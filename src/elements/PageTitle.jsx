import React from "react";

class PageTitle extends React.Component {
    render() {
        return (
            <div className="page-header row no-gutters py-4 mb-3 border-bottom">
                <div className="col-12 col-sm-4 text-center text-sm-left mb-0">
                    {this.props.subtitle && <span className="text-uppercase page-subtitle">{this.props.subtitle}</span>}
                    <h3 className="page-title">{this.props.title}</h3>
                </div>
            </div>
        )
    }
}

export default PageTitle