import React from "react";

class DangerAlert extends React.Component {
    render() {
        return (
            <div
                className={'alert alert-danger notification-alert text-center'}
                dangerouslySetInnerHTML={{__html: this.props.content}}
            />
        )
    }
}

export default DangerAlert;