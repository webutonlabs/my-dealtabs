import React from "react";

class WarningAlert extends React.Component {
    render() {
        return (
            <div
                className={'alert alert-warning notification-alert text-center'}
                dangerouslySetInnerHTML={{__html: this.props.content}}
            />
        )
    }
}

export default WarningAlert;