import React from "react";

import {MdClose} from "react-icons/md";

class NotificationAlert extends React.Component {
    getClass(type) {
        let classes = `notification-alert text-center alert mb-1 `;

        if (type === 'success') {
            return classes + 'alert-success';
        }

        if (type === 'accent') {
            return classes + 'alert-accent';
        }

        if (type === 'info') {
            return classes + 'alert-info';
        }

        if (type === 'danger') {
            return classes + 'alert-danger';
        }

        if (type === 'warning') {
            return classes + 'alert-warning';
        }

        return classes;
    }

    render() {
        return (this.props.text ?
                <div className={this.getClass(this.props.type)} role="alert">
                    <span className="mobile-text text-white fw-500">
                        {this.props.subject && `(${this.props.subject}) `}
                        {this.props.text}
                    </span>
                    {this.props.dismissible &&
                        <button type="button" className="close" onClick={this.props.dismissibleCallback}>
                            <span aria-hidden="true" className="fw-500 text-white"><MdClose/></span>
                        </button>
                    }
                </div> :
                <div className={this.getClass(this.props.type)} role="alert">
                    {this.props.dismissible &&
                        <button type="button" className="close" >
                            <span aria-hidden="true" className="fw-500 text-white"><MdClose/></span>
                        </button>
                    }
                    {this.props.children}
                </div>
        )
    }
}

export default NotificationAlert;