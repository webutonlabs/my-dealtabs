import React from "react";

class MainContent extends React.Component {
    render() {
        return (
            <div className="main-content-container container-fluid px-4">
                {this.props.children}
            </div>
        )
    }
}

export default MainContent