import React from "react";
import { Nav } from "shards-react";

import SidebarNavItem from "./SidebarNavItem";
import {FaBuffer, FaUser, FaDoorOpen, FaMoneyCheck, FaSync, FaArchive, FaQuestionCircle} from "react-icons/all";

class SidebarNavItems extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      navItems: [
        {
          title: "Sessions",
          to: "/sessions",
          icon: <FaBuffer />
        },
        {
          title: "live-sync",
          to: "/live-sync",
          icon: <FaSync />
        },
        {
          title: "archived-tabs",
          to: "/archived-tabs",
          icon: <FaArchive />
        },
        {
          title: "Membership",
          to: "/membership",
          icon: <FaMoneyCheck />
        },
        {
          title: "Settings",
          to: "/user",
          icon: <FaUser />
        },
        {
          title: "Support center",
          to: process.env.REACT_APP_SITE_HOST + `/contact-us`,
          icon: <FaQuestionCircle />,
          pure: true
        },
        {
          title: "Logout",
          to: "/logout",
          icon: <FaDoorOpen />
        }
      ]
    };
  }

  render() {
    const { navItems: items } = this.state;
    return (
      <div className="nav-wrapper">
        <Nav className="nav--no-borders flex-column">
          {items.map((item, idx) => (
            <SidebarNavItem key={idx} item={item} />
          ))}
        </Nav>
      </div>
    )
  }
}

export default SidebarNavItems;
