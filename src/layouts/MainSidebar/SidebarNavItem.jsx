import React from "react";
import { NavLink as RouteNavLink } from "react-router-dom";
import { NavItem, NavLink } from "shards-react";
import {useTranslation} from "react-i18next";

const SidebarNavItem = ({ item }) => {
    const {t} = useTranslation();

    return (
        <NavItem>
            {item.pure !== undefined &&
                <a className={'nav-link'} href={item.to}>
                    <div className="text-center sidebar-item">
                        <span>{item.icon}</span>
                        <span className="ml-05">{t(item.title)}</span>
                    </div>
                </a>
            }
            {item.pure === undefined &&
                <NavLink tag={RouteNavLink} to={item.to}>
                    <div className="text-center sidebar-item">
                        <span>{item.icon}</span>
                        <span className="ml-05">{t(item.title)}</span>
                    </div>
                </NavLink>
            }
        </NavItem>
    )
};

export default SidebarNavItem;
