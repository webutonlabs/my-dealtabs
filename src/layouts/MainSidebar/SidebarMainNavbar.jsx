import React from "react";
import {Navbar} from "shards-react";

import {Dispatcher, Constants} from '../../flux';
import {FaAlignJustify} from "react-icons/fa";
import {AppContext} from "../../components/App/AppContext";

class SidebarMainNavbar extends React.Component {
    static contextType = AppContext;

    handleToggleSidebar = () => {
        Dispatcher.dispatch({actionType: Constants.TOGGLE_SIDEBAR});
    }

    render() {
        return (
            <div className="main-navbar">
                <Navbar className=" bg-white border-bottom p-0" type="light">
                    <div className="container-fluid">
                        <div className="row text-center w-100">
                            <a className="w-100 mr-0" href={process.env.REACT_APP_SITE_HOST + '/' + this.context.user.locale} style={{lineHeight: "20px"}}>
                                <h5 className="d-md-inline ml-30 text-dark">
                                    DealTabs
                                </h5>
                            </a>
                        </div>
                    </div>
                    <a className="toggle-sidebar d-sm-inline d-md-none d-lg-none pb-0-pb-16" onClick={this.handleToggleSidebar}>
                        <FaAlignJustify/>
                    </a>
                </Navbar>
            </div>
        );
    }
}

export default SidebarMainNavbar;
