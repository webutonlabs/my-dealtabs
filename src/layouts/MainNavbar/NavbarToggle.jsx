import React from "react";

import {Dispatcher, Constants} from '../../flux';
import {FaAlignJustify} from "react-icons/fa";
import {AppContext} from "../../components/App/AppContext";
import {isUserPremium} from "../../infrastructure/helpers";

class NavbarToggle extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        return (
        <>
            <span>{this.context.user.email}</span>
            <span className="badge badge-primary box-shadow ml-1 fw-500">
                        {isUserPremium(this.context.user) ? t('Premium') : t('Basic')}
                    </span>
            <a style={{marginRight: '25px'}} href="#" onClick={() => {Dispatcher.dispatch({actionType: Constants.TOGGLE_SIDEBAR})}}
               className="toggle-sidebar-svg nav-link nav-link-icon toggle-sidebar d-sm-inline d-md-inline d-lg-none text-center">
                <FaAlignJustify/>
            </a>
        </>
    )
    }
}

export default NavbarToggle;
