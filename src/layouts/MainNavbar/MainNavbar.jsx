import React from "react";
import {Container} from "shards-react";

import NavbarToggle from "./NavbarToggle";

const MainNavbar = () => {
  return (
    <div className="main-navbar bg-white">
      <Container className="p-0">
          <nav className="navbar navbar-light bg-light mt-0 p-0">
              <ul className="navbar-nav mr-auto"></ul>
              <form className="form-inline my-lg-0">
                  <NavbarToggle />
              </form>
          </nav>
          </Container>
    </div>
  );
};

export default MainNavbar;
