import React from "react";
import { Container, Row, Col } from "shards-react";

import MainSidebar from "./MainSidebar/MainSidebar";
import MainNavbar from "./MainNavbar/MainNavbar";
import {ToastContainer} from "react-toastify";
import Notifications from "../components/Notification/Notifications";

const DashboardLayout = ({ children }) => (
    <Container fluid className="mobile-no-x-padding">
        <Row>
            <MainSidebar />
            <Col
                className="main-content p-0"
                lg={{ size: 10, offset: 2 }}
                md={{ size: 9, offset: 3 }}
                sm="12"
                tag="main"
            >
                <MainNavbar />
                <Notifications />
                <ToastContainer />
                {children}
            </Col>
        </Row>
    </Container>
);

export default DashboardLayout;
