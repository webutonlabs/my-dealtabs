import React from "react";
import SortableSessionWindowList from "./SortableSessionWindowList";
import DisabledSessionWindowsList from "./DisabledSessionWindowsList";
import {filterByEnableStatus} from "../../../../infrastructure/helpers";

class SessionWindowList extends React.Component {
    render() {
        const [enabledWindows, disabledWindows] = filterByEnableStatus(this.props.windows);

        let itemIndex = 0;
        for (const window of enabledWindows) {
            window.itemIndex = ++itemIndex;
        }

        for (const window of disabledWindows) {
            window.itemIndex = ++itemIndex;
        }

        return (
            <>
                <SortableSessionWindowList
                    windows={enabledWindows}
                    session={this.props.session}
                    getSessions={this.props.getSessions}
                    updateSessions={this.props.updateSessions}
                />
                <DisabledSessionWindowsList
                    windows={disabledWindows}
                    session={this.props.session}
                    getSessions={this.props.getSessions}
                    updateSessions={this.props.updateSessions}
                />
            </>
        )
    }
}

export default SessionWindowList;