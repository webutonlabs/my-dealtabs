import React from "react";
import SessionWindow from "../SessionWindow";
import NotificationAlert from "../../../../elements/NotificationAlert";
import {AppContext} from "../../../App/AppContext";

class DisabledSessionWindowsList extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        return (
            this.props.windows.map(window => (
                <div className="card mt-10" key={window.id}>
                    <NotificationAlert type="warning">
                        <span className="fw-500 mobile-text">{t('notification-disabled-window-1')} </span>
                    </NotificationAlert>
                    <SessionWindow
                        getSessions={this.props.getSessions}
                        updateSessions={this.props.updateSessions}
                        window={window}
                        session={this.props.session}
                    />
                </div>
            ))
        )
    }
}

export default DisabledSessionWindowsList;