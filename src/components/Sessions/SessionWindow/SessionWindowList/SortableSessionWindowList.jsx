import React from "react";
import {SortableContainer, SortableElement} from "react-sortable-hoc";
import SessionWindow from "../SessionWindow";
import {filterByEnableStatus, makeArrayOfIds} from "../../../../infrastructure/helpers";
import arrayMove from "array-move";
import {AppContext} from "../../../App/AppContext";
import AppLoader from "../../../../elements/Loader/AppLoader";
import apiClient from "../../../../infrastructure/Api/apiClient";

class SortableSessionWindowList extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {isLoading: false};
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        if (oldIndex !== newIndex) {
            const newOrder = arrayMove(this.props.windows, oldIndex, newIndex);

            this.setState({isLoading: true});
            apiClient(this.context.token, this.context.deviceId).patch(`/sessions/${this.props.session.id}/order-windows`, {'windows': makeArrayOfIds(newOrder)})
                .then(() => {
                    let sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            const [enabledWindows, disabledWindows] = filterByEnableStatus(sessions[i].windows);
                            sessions[i].windows = [...newOrder, ...disabledWindows];

                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                    this.setState({isLoading: false});
                })
        }
    };

    render() {
        const SortableItem = SortableElement(({window}) => {
                return (
                    <div className="card mt-10">
                        <SessionWindow
                            window={window}
                            session={this.props.session}
                            getSessions={this.props.getSessions}
                            updateSessions={this.props.updateSessions}
                        />
                    </div>
                )
            }
        );

        const SortableList = SortableContainer(({windows}) => {
            return (
                <div>
                    {
                        windows.map((window, index) => (
                            <SortableItem key={`window-${window.id}`} index={index} window={window}/>
                        ))
                    }
                </div>
            );
        });

        return this.state.isLoading ? <AppLoader /> :
            <SortableList pressDelay={150} windows={this.props.windows} onSortEnd={this.onSortEnd}/>;
    }
}

export default SortableSessionWindowList;