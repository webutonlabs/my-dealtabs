import React from "react";
import {NotBlank} from "../../../infrastructure/FormBuilder/validators";
import FormBuilder from "../../../infrastructure/FormBuilder/FormBuilder";
import AppLoader from "../../../elements/Loader/AppLoader";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/apiClient";

class SessionWindowEditForm extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                isValid: true,
                children: {
                    name: {
                        label: 'Session window name',
                        type: 'text',
                        value: props.window.name,
                        validators: [
                            new NotBlank('Session window name cannot be empty')
                        ]
                    },
                    session: {
                        type: 'select',
                        label: 'Session',
                        data: [],
                        value: props.session.id.toString(),
                        translate: false
                    }
                }
            }
        }
    }

    componentDidMount() {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).get(`/sessions?per_page=9999`).then(res => {
            let form = this.state.form;
            form.children.session.data = res.data;

            this.setState({
                form: form,
                isLoading: false
            })
        })
    }

    onSave = (e) => {
        e.preventDefault();

        if (this.state.form.isValid) {
            this.setState({isLoading: true});

            const data = {name: this.state.form.children.name.value};

            if (Number(this.state.form.children.session.value) !== this.props.session.id) {
                data.session = this.state.form.children.session.value;
            }

            apiClient(this.context.token, this.context.deviceId).patch(`/session-windows/${this.props.window.id}`, data).then((res) => {
                if (this.props.session.id.toString() !== this.state.form.children.session.value) {
                    window.location.href = `/sessions?toast=success&message=${this.context.t('changes-saved')}`
                } else {
                    const sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            for (let j = 0; j < sessions[i].windows.length; j++) {
                                if (sessions[i].windows[j].id === this.props.window.id) {
                                    sessions[i].windows[j] = res.data;

                                    break;
                                }
                            }

                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                }
            })
        }
    }

    render() {
        return (
            this.state.isLoading
                ? <AppLoader/>
                : <FormBuilder form={this.state.form} onSave={this.onSave} onCancel={this.props.toggleEdit} />
        )
    }
}

export default SessionWindowEditForm;