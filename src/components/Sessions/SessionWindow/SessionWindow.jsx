import React from "react";
import NoneTabs from "../WindowTab/NoneTabs";
import SessionWindowEditForm from "./SessionWindowEditForm";
import AppLoader from "../../../elements/Loader/AppLoader";
import {AppContext} from "../../App/AppContext";
import WindowTabList from "../WindowTab/WindowTabList/WindowTabList";
import apiClient from "../../../infrastructure/Api/apiClient";

class SessionWindow extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            onEdit: false
        }
    }

    toggleEdit = () => {
        this.setState({onEdit: !this.state.onEdit});
    }

    deleteSessionWindow = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).delete(`/session-windows/${this.props.window.id}`)
            .then(() => {
                this.setState({isLoading: false})

                const sessions = this.props.getSessions();

                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        for (let j = 0; j < sessions[i].windows.length; j++) {
                            if (sessions[i].windows[j].id === this.props.window.id) {
                                sessions[i].windows.splice(j, 1);

                                break;
                            }
                        }
                    }
                }

                this.props.updateSessions(sessions);
            })
    }

    render() {
        const t = this.context.t;
        const isEnabled = this.props.window['is_enabled'];
        const btnClasses = `btn btn-white ${isEnabled ? '' : 'disabled'}`;

        return (
            this.state.isLoading ? <AppLoader/> :
                <div className="card-body border-bottom text-center mobile-x-padding-5">
                    {!this.state.onEdit ?
                        <div className="mobile-text">
                            {this.props.window.name === null ? `${t('Session window')} # ${this.props.window.itemIndex}` : this.props.window.name}
                        </div> : <SessionWindowEditForm
                            getSessions={this.props.getSessions}
                            updateSessions={this.props.updateSessions}
                            session={this.props.session}
                            window={this.props.window}
                            toggleEdit={this.toggleEdit}
                        />
                    }
                    {!this.state.onEdit &&
                    <div className="btn-group btn-group-toggle mt-2" data-toggle="buttons">
                        <label onClick={isEnabled ? this.toggleEdit : () => {}} className={btnClasses}>
                            <input type="radio" autoComplete="off"/>
                            <span className="mobile-text">{t('Edit')}</span>
                        </label>
                        <label className="btn btn-danger">
                            <input onClick={this.deleteSessionWindow} type="radio" name="options" id="option3"
                                   autoComplete="off"/> <span className="mobile-text">{t('Delete')}</span>
                        </label>
                    </div>
                    }
                    {this.props.window.tabs.length > 0
                        ? <WindowTabList
                            tabs={this.props.window.tabs}
                            session={this.props.session}
                            window={this.props.window}
                            getSessions={this.props.getSessions}
                            updateSessions={this.props.updateSessions}
                        /> : <NoneTabs/>
                    }
                </div>
        )
    }
}

export default SessionWindow;