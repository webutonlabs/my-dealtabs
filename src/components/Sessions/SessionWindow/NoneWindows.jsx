import React from "react";
import {AppContext} from "../../App/AppContext";

class NoneWindows extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        return <h3 className="mt-30">{t('None session windows saved...')}</h3>
    }
}

export default NoneWindows;