import React from "react";
import {Modal} from "react-bootstrap";
import {AppContext} from "../../App/AppContext";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import {FaUserPlus} from "react-icons/fa";
import {errorToast, successToast} from "../../../infrastructure/toast";
import apiClient from "../../../infrastructure/Api/apiClient";

class AddCollaboratorModal extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            similarEmails: [],
            userNotFound: false
        };
    }

    handleEmailChange = (e) => {
        this.setState({email: e.currentTarget.value});
    }

    sendNotRegisteredInvitation = (email) => {
        apiClient(this.context.token, this.context.deviceId)
            .post('/collaboration/invite', {
                email: email,
                session: this.props.session.id,
                invite_not_registered: true
            })
            .then(res => {
                res.status === 200
                    ? successToast(this.context.t('invitation-successfully-resent-to') + ` ${email}`)
                    : successToast(this.context.t('invitation-successfully-sent-to') + ` ${email}`);

                this.setState({userNotFound: false, email: '', similarEmails: []});
                this.props.toggleModal();
            })
            .catch(error => {
                if (error.response.data.errors[0].pointer === 'email') {
                    errorToast(this.context.t('invalid-email'));
                }
            });
    }

    sendInvitation = () => {
        apiClient(this.context.token, this.context.deviceId)
            .post('/collaboration/invite', {email: this.state.email, session: this.props.session.id})
            .then(res => {
                res.status === 200
                    ? successToast(this.context.t('invitation-successfully-resent-to') + ` ${this.state.email}`)
                    : successToast(this.context.t('invitation-successfully-sent-to') + ` ${this.state.email}`);

                this.setState({userNotFound: false, email: ''});
                this.props.toggleModal();
            })
            .catch(error => {
                if (error.response.status === 404) {
                    errorToast(this.context.t('user-with-such-email-not-found'));
                    this.setState({similarEmails: error.response.data, userNotFound: true});
                } else if (error.response.status === 400 && error.response.data.errors) {
                    if (error.response.data.errors[0].pointer === 'email') {
                        errorToast(this.context.t('invalid-email'));
                    }
                } else {
                    errorToast(this.context.t('you-are-the-owner'));
                }
            });
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.toggleModal} size={'lg'}>
                <Modal.Header closeButton>
                    {this.props.session.name}
                </Modal.Header>
                <Modal.Body>
                    <div className={'row'}>
                        <div className={'col-8 col-md-9 col-lg-9'}>
                            <input onChange={this.handleEmailChange} value={this.state.email} type={'text'} className={'form-control'} placeholder={'Email'}/>
                        </div>
                        <div className={'col-3 text-center'}>
                            <button onClick={this.sendInvitation} className={'btn btn-sm btn-primary text-white'}>{this.context.t('send-invitation')}</button>
                        </div>
                    </div>
                        <div className={'row mt-3'}>
                            {this.state.userNotFound &&
                                <div className={'col-12 text-center'}>
                                    <button onClick={() => {this.sendNotRegisteredInvitation(this.state.email)}} className={'btn btn-success text-white'}>{this.context.t('invite-not-registered')}</button>
                                </div>
                            }
                            {this.state.similarEmails.length > 0 &&
                                <>
                                    <div className={'col-12 mt-3'}>
                                        <div className={'alert alert-warning'}>
                                            <span>{this.context.t('maybe-you-meant')}</span>
                                        </div>
                                    </div>
                                    <div className={'col-12'}>
                                        <ul className="list-group collaborated-users-list" style={{borderRadius: 0}}>
                                            {this.state.similarEmails.map(similarEmail => {
                                                return <li key={similarEmail} className={`p-2 list-group-item d-flex justify-content-between align-items-center`}>
                                                    <div>
                                                        <span className={`tab-name fw-600`}>{similarEmail}</span>
                                                    </div>
                                                    <ButtonGroup classname={'w-100'}>
                                                        <button onClick={() => {this.sendNotRegisteredInvitation(similarEmail)}} className={`btn btn-light btn-sm`} >
                                                            <FaUserPlus />
                                                        </button>
                                                    </ButtonGroup>
                                                </li>
                                            })}
                                        </ul>
                                    </div>
                                </>
                            }
                        </div>
                </Modal.Body>
            </Modal>
        )
    }
}

export default AddCollaboratorModal;