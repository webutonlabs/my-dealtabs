import React from "react";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import {FaUserSlash} from "react-icons/fa";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/apiClient";
import {successToast} from "../../../infrastructure/toast";

class CollaboratedUser extends React.Component {
    static contextType = AppContext;

    deleteCollaborator = () => {
        apiClient(this.context.token, this.context.deviceId)
            .patch(`/collaboration/delete-collaborator`, {email: this.props.user['email'], session: this.props.session.id}).then(res => {
            let users = this.props.users;
            for (let i = 0; i < users.length; i++) {
                if (users[i].email === this.props.user.email) {
                    users.splice(i, 1);
                    break;
                }
            }

            if (!this.props.session.owned) { // session will be removed from user
                const sessions = this.props.getSessions();
                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        sessions.splice(i, 1);
                        break;
                    }
                }

                this.props.updateSessions(sessions);
            }

            this.props.setUsers(users);
            successToast(this.context.t('successfully-deleted'));
        })
    }

    render() {
        const isOwner = this.props.user['is_owner'];
        let canDelete = true;
        if (!this.props.session.owned && (this.props.user.email !== this.context.user.email)) {
            canDelete = false;
        }

        return (
            <li className={`p-3 list-group-item d-flex justify-content-between align-items-center ${isOwner ? 'owner' : ''}`}>
                <div>
                    <span className={`tab-name fw-600`}>{this.props.user.email}</span>
                    {isOwner &&
                        <span className={'ml-05 badge badge-primary'}>{this.context.t('owner')}</span>
                    }
                    {!this.props.user['is_confirmed'] &&
                        <span className={'ml-05 badge badge-success'}>{this.context.t('invited')}</span>
                    }
                </div>
                <ButtonGroup classname={'w-100'}>
                    {!isOwner && canDelete &&
                        <button onClick={this.deleteCollaborator} className={`text-danger btn btn-light`} >
                            <FaUserSlash/>
                        </button>
                    }
                </ButtonGroup>
            </li>
        )
    }
}

export default CollaboratedUser;