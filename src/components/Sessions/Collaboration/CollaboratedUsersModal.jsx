import React from "react";
import {Modal} from "react-bootstrap";
import CollaboratedUsersList from "./CollaboratedUsersList";
import {AppContext} from "../../App/AppContext";

class CollaboratedUsersModal extends React.Component {
    static contextType = AppContext;

    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.toggleModal} size={'lg'}>
                <Modal.Header closeButton style={{borderBottom: 'none'}}>
                    {this.props.session.name}
                </Modal.Header>
                <Modal.Body className={'p-0'}>
                    <CollaboratedUsersList
                        session={this.props.session}
                        users={this.props.users}
                        setUsers={this.props.setUsers}
                        getSessions={this.props.getSessions}
                        updateSessions={this.props.updateSessions}
                    />
                </Modal.Body>
            </Modal>
        )
    }
}

export default CollaboratedUsersModal;