import React from "react";
import CollaboratedUser from "./CollaboratedUser";

class CollaboratedUsersList extends React.Component {
    render() {
        return (
            <ul className="list-group collaborated-users-list" style={{borderRadius: 0}}>
                {this.props.users.map(user => {
                    return (
                        <CollaboratedUser
                            key={user.id}
                            user={user}
                            session={this.props.session}
                            users={this.props.users}
                            setUsers={this.props.setUsers}
                            getSessions={this.props.getSessions}
                            updateSessions={this.props.updateSessions}
                        />
                    )
                })}
            </ul>
        )
    }
}

export default CollaboratedUsersList;