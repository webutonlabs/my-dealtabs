import React from 'react'
import PageTitle from "../../elements/PageTitle";
import MainContent from "../../elements/MainContent";
import {AppContext} from "../App/AppContext";
import NoneSessions from "./Session/NoneSessions";
import Pagination from "./Pagination";
import NotificationAlert from "../../elements/NotificationAlert";
import {Constants, Dispatcher, Store} from "../../flux";
import AppLoader from "../../elements/Loader/AppLoader";
import SessionsList from "./Session/SessionList/SessionsList";
import {isUserPremium} from "../../infrastructure/helpers";
import apiClient from "../../infrastructure/Api/apiClient";
import {ToastContainer} from "react-toastify";
import {toastFromQuery} from "../../infrastructure/toast";

class SessionsComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            sessions: [],
            perPage: 6,
            page: 1,
            nextPage: null,
            totalSessions: 0,
            sessionsWithNonCollapsedWindows: []
        }
    }

    loadMore = () => {
        if (null !== this.state.nextPage) {
            this.fetchSessions();
        }
    }

    getSessionsWithNonCollapsedWindows = () => {
        return this.state.sessionsWithNonCollapsedWindows;
    }

    updateSessionsWithNonCollapsedWindows = (sessionsWithNonCollapsedWindows) => {
        this.setState({sessionsWithNonCollapsedWindows: sessionsWithNonCollapsedWindows});
    }

    componentDidMount() {
        document.title = this.context.t('title-my-dealtabs-sessions');
        toastFromQuery();

        if (Store.getMenuState() === true) {
            Dispatcher.dispatch({actionType: Constants.TOGGLE_SIDEBAR});
        }

        this.fetchSessions(this.state.offset);
    }

    fetchSessions() {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).get(`/sessions?relations=windows,tabs&per_page=${this.state.perPage}&page=${this.state.page}&all=true`)
            .then(res => {
                let sessions = [...this.state.sessions, ...res.data];

                this.setState({
                    isLoading: false,
                    page: this.state.page + 1,
                    nextPage: res.headers['x-pagination-next-page'] === 'null' ? null : Number(res.headers['x-pagination-next-page']),
                    sessions: sessions,
                    totalSessions: res.headers['x-pagination-total']
                });

                if (this.state.page > 1) {
                    window.scrollTo(0, document.body.scrollHeight);
                }
            })
    }

    updateSessions = (sessions) => {
        this.setState({sessions: sessions});
    }

    getSessions = () => {
        return this.state.sessions;
    }

    render() {
        const t = this.context.t;

        return (
            this.state.isLoading ? <AppLoader/> :
                this.state.sessions.length > 0 ?
                    <>
                        {!isUserPremium(this.context.user) &&
                            <>
                                <NotificationAlert type="accent">
                                    <span className="fw-500 mobile-text">{t('notification-basic-membership')} </span>
                                    <a href={'/membership'} className="text-white fw-500 mobile-text"><span>{t('notification-basic-membership-1')}</span></a>
                                    <span className="fw-500 mobile-text"> {t('notification-basic-membership-2')}</span>
                                </NotificationAlert>
                            </>
                        }
                        <MainContent>
                            <PageTitle title={t('Manage sessions')} subtitle={t('Sessions')}/>
                            <SessionsList
                                getSessionsWithNonCollapsedWindows={this.getSessionsWithNonCollapsedWindows}
                                updateSessionsWithNonCollapsedWindows={this.updateSessionsWithNonCollapsedWindows}
                                sessions={this.state.sessions}
                                getSessions={this.getSessions}
                                updateSessions={this.updateSessions}
                            />
                            {null !== this.state.nextPage && <Pagination loadMore={this.loadMore}/>}
                        </MainContent>
                    </> : <NoneSessions/>
        )
    };
}

export default SessionsComponent