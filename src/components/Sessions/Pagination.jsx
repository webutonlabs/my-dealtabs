import React from "react";

class Pagination extends React.Component {
    render() {
        return (
            <div className="row mb-2 mt-2">
                <div className="container col-12">
                    <div className="btn-group btn-group-toggle" data-toggle="buttons">
                        <label className={'btn btn-lg btn-white'}>
                            <input onClick={this.props.loadMore} type="radio" name="options" autoComplete="off"/> Load more
                        </label>
                    </div>
                </div>
            </div>
        )
    }
}

export default Pagination