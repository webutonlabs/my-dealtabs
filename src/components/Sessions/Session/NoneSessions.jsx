import React from "react";
import NotificationAlert from "../../../elements/NotificationAlert";
import MainContent from "../../../elements/MainContent";
import PageTitle from "../../../elements/PageTitle";
import {AppContext} from "../../App/AppContext";
import {FaFirefoxBrowser, FaChrome, FaOpera} from "react-icons/all";

class NoneSessions extends React.Component {
    static contextType = AppContext;

    chrome = () => {
        window.location.href = 'https://chrome.google.com/webstore/detail/dealtabs-browser-tab-orga/hfojefnbjfhacmgjnoijcbmakplknmhk/?hl=' + this.context.user.locale;
    }

    firefox = () => {
        window.location.href = `https://addons.mozilla.org/${this.context.user.locale}/firefox/addon/dealtabs/`
    }

    opera = () => {
        window.location.href = `https://addons.opera.com/${this.context.user.locale}/extensions/details/install-chrome-extensions/`
    }

    render() {
        const t = this.context.t;

        return (
            <>
                <NotificationAlert type="accent" text={t('no-sessions-message')}/>
                <MainContent>
                    <PageTitle title={t('Download extension')} subtitle={t('try-save-session')}/>
                        <div className="row">
                            <div className={'container-fluid'}>
                                <div className="btn-group btn-group-toggle mt-2" data-toggle="buttons">
                                    <label onClick={this.chrome} className="btn btn-white">
                                        <input type="radio" autoComplete="off"/>
                                        <span className="mobile-text" style={{'font-size': '40px'}}><FaChrome/></span>
                                    </label>
                                    <label onClick={this.firefox} className="btn btn-white">
                                        <input type="radio" autoComplete="off"/>
                                        <span className="mobile-text" style={{'font-size': '40px'}}><FaFirefoxBrowser/></span>
                                    </label>
                                    <label onClick={this.opera} className="btn btn-white">
                                        <input type="radio" autoComplete="off"/>
                                        <span className="mobile-text" style={{'font-size': '40px'}}><FaOpera/></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                </MainContent>
            </>
        )
    }
}

export default NoneSessions