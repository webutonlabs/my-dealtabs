import React from "react";
import {MdContentCopy, MdKeyboardArrowDown, MdKeyboardArrowUp} from "react-icons/md";
import NoneWindows from "../SessionWindow/NoneWindows";
import SessionEditForm from "./SessionEditForm";
import AppLoader from "../../../elements/Loader/AppLoader";
import {AppContext} from "../../App/AppContext";
import NotificationAlert from "../../../elements/NotificationAlert";
import SessionWindowList from "../SessionWindow/SessionWindowList/SessionWindowList";
import {CopyToClipboard} from "react-copy-to-clipboard";
import {isUserPremium as isPremium} from "../../../infrastructure/helpers";
import apiClient from "../../../infrastructure/Api/apiClient";
import {default as dateFormat} from "dateformat";
import {FaToggleOff, FaToggleOn, FaUserPlus, FaUsers} from "react-icons/all";
import {errorToast} from "../../../infrastructure/toast";
import AddCollaboratorModal from "../Collaboration/AddCollaboratorModal";
import CollaboratedUsersModal from "../Collaboration/CollaboratedUsersModal";

class SessionCard extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            windowsCollapsed: !props.getSessionsWithNonCollapsedWindows().includes(props.session.id),
            onEdit: false,
            isLoading: false,
            onShare: false,
            shareReference: null,
            shareLinkCopied: false,
            showCollaboratedUsersModal: false,
            showAddCollaboratorModal: false,
            collaboratedUsers: []
        }
    }

    toggleOnShare = () => {
        if (this.state.shareReference === null) {
            this.setState({isLoading: true});

            apiClient(this.context.token, this.context.deviceId).post(`/sessions/${this.props.session.id}/share`, {})
                .then(res => {
                    this.setState({
                        shareReference: res.data['reference_id'],
                        onShare: true,
                        isLoading: false
                    })
                })
        } else {
            this.setState({onShare: !this.state.onShare});
        }
    }

    deleteSession = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).delete(`/sessions/${this.props.session.id}`)
            .then(() => {
                this.setState({isLoading: false});

                const sessions = this.props.getSessions();
                sessions.forEach((session, index) => {
                    if (this.props.session.id === session.id) {
                        sessions.splice(index, 1);
                    }
                })

                this.props.updateSessions(sessions);
            })
    }

    collapseWindows = () => {
        const sessionsWithNonCollapsedWindows = this.props.getSessionsWithNonCollapsedWindows();

        if (sessionsWithNonCollapsedWindows.includes(this.props.session.id)) {
            for (let i = 0; i < sessionsWithNonCollapsedWindows.length; i++) {
                if (sessionsWithNonCollapsedWindows[i] === this.props.session.id) {
                    sessionsWithNonCollapsedWindows.splice(i, 1);

                    break;
                }
            }
        } else {
            sessionsWithNonCollapsedWindows.push(this.props.session.id);
        }

        this.props.updateSessionsWithNonCollapsedWindows(sessionsWithNonCollapsedWindows);

        this.setState({windowsCollapsed: !this.state.windowsCollapsed})
    }

    toggleEdit = () => {
        this.setState({onEdit: !this.state.onEdit});
    }

    toggleShareLinkCopied = () => {
        this.setState({shareLinkCopied: true});

        setTimeout(() => {
            this.setState({shareLinkCopied: false})
        }, 2000)
    }

    toggleCollaboration = () => {
        if (!isPremium(this.context.user)) {
            errorToast(this.context.t('premium-feature'))
            return;
        }

        this.setState({isLoading: true});
        apiClient(this.context.token, this.context.deviceId).patch('/collaboration', {session: this.props.session.id}).then(res => {
            const sessions = this.props.getSessions();
            for (let i = 0; i < sessions.length; i++) {
                if (this.props.session.id === sessions[i].id) {
                    sessions[i]['enabled_for_collaboration'] = !sessions[i]['enabled_for_collaboration'];
                    break;
                }
            }

            this.setState({isLoading: false});
            this.props.updateSessions(sessions);
        })
    }

    setCollaboratedUsers = (users) => {
        this.setState({collaboratedUsers: users});
    }

    toggleCollaboratedUsersModal = () => {
        this.setState({showCollaboratedUsersModal: !this.state.showCollaboratedUsersModal});
    }

    showCollaboratedUsersModal = () => {
        apiClient(this.context.token, this.context.deviceId).get(`/collaboration?session=${this.props.session.id}`).then(res => {
            this.setCollaboratedUsers(res.data);
            this.setState({showCollaboratedUsersModal: true});
        });
    }

    toggleAddCollaboratorModal = () => {
        this.setState({showAddCollaboratorModal: !this.state.showAddCollaboratorModal});
    }

    showAddCollaboratorModal = () => {
        this.setState({showAddCollaboratorModal: true});
    }

    render() {
        const isUserPremium = isPremium(this.context.user);
        const t = this.context.t;

        const sessionEnabled = this.props.session['is_enabled'];
        const thisDeviceSession = this.props.session['device_id'] === this.context.deviceId;
        const collapseClasses = `collapse ${this.state.windowsCollapsed ? '' : 'show'}`;
        const btnClasses = `btn btn-white ${((!sessionEnabled || !thisDeviceSession) && !isUserPremium) ? 'disabled' : ''}`;
        const isCollaborated = (this.props.session['enabled_for_collaboration'] || !this.props.session.owned)
        const isOwned = this.props.session.owned;

        let isCollaborationEnabled = sessionEnabled;
        if (this.props.session.owned && (!isUserPremium || !this.props.session['enabled_for_collaboration'])) {
            isCollaborationEnabled = false;
        }

        return (
            this.state.isLoading ? <AppLoader/> :
                <div className="card card-small mb-4">
                    {(!sessionEnabled || !thisDeviceSession) && !isUserPremium && isOwned &&
                    <NotificationAlert type="warning">
                        <span className="fw-500 mobile-text">
                            {t(thisDeviceSession ? 'notification-disabled-session-1' : 'notification-disabled-session-2')}
                        </span>
                    </NotificationAlert>
                    }
                    <div className="card-header border-bottom text-center mobile-no-x-padding">
                        {this.state.onEdit ? <SessionEditForm
                                getSessions={this.props.getSessions}
                                updateSessions={this.props.updateSessions}
                                session={this.props.session}
                                toggleEdit={this.toggleEdit}
                            /> :
                            <>
                                <div className={((!sessionEnabled || !thisDeviceSession) && !isUserPremium && isOwned) ? 'text-reagent-grey px-3' : 'text-dark px-3'}>
                                    {this.props.session.name}
                                    {isCollaborated &&
                                        <span className={'shared-msg badge badge-success ml-2'}>{this.context.t('collaborated')}</span>
                                    }
                                </div>
                                <div>
                                    <span className="mobile-text">{dateFormat(this.props.session['creation_date'], 'mmmm dd, yyyy HH:MM')}</span>
                                </div>
                                <div>
                                    <div className="btn-group btn-group-toggle mt-2" data-toggle="buttons">
                                        <label
                                            onClick={((!sessionEnabled || !thisDeviceSession) && !isUserPremium && isOwned) ? () => {
                                            } : this.toggleOnShare} className={btnClasses}>
                                            <input type="radio" autoComplete="off"/> <span
                                            className="mobile-text">{t('Share')}</span> </label>
                                        <label
                                            onClick={((!sessionEnabled || !thisDeviceSession) && !isUserPremium && isOwned) ? () => {
                                            } : this.toggleEdit} className={btnClasses}>
                                            <input type="radio" autoComplete="off"/> <span
                                            className="mobile-text">{t('Rename')}</span>
                                        </label>
                                        <label onClick={this.deleteSession} className="btn btn-danger">
                                            <input type="radio" autoComplete="off"/> <span
                                            className="mobile-text">{t('Delete')}</span>
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <div className="btn-group btn-group-toggle mt-2 px-2" data-toggle="buttons">
                                        <label onClick={!isCollaborationEnabled ? () => {} : this.showAddCollaboratorModal} className={`btn btn-white ${isCollaborationEnabled ? '' : 'disabled'}`}>
                                            <input type="radio" autoComplete="off"/>
                                            <FaUserPlus />
                                            <span className="mobile-text ml-05">{t('add-collaborator')}</span>
                                        </label>
                                        <label onClick={!isCollaborationEnabled ? () => {} : this.showCollaboratedUsersModal} className={`btn btn-white ${isCollaborationEnabled ? '' : 'disabled'}`}>
                                            <input type="radio" autoComplete="off"/>
                                            <FaUsers />
                                            <span className="mobile-text ml-05">{t('collaborated-users')}</span>
                                        </label>
                                        {this.props.session.owned && sessionEnabled &&
                                            <label onClick={this.toggleCollaboration} className={`btn ${this.props.session['enabled_for_collaboration'] ? 'btn-success' : 'btn-danger'}`}>
                                                <input type="radio" autoComplete="off"/>
                                                <span className="mobile-text">
                                                    {t('collaboration')}
                                                    {this.props.session['enabled_for_collaboration']
                                                        ? <FaToggleOn className={'ml-05'} />
                                                        : <FaToggleOff className={'ml-05'} />
                                                    }
                                                </span>
                                            </label>
                                        }
                                    </div>
                                </div>
                                {this.state.onShare &&
                                    <div className={'mt-10'}>
                                        <input className={'form-control'} type={'text'}
                                               value={`${process.env.REACT_APP_SITE_HOST}/${this.context.user.locale}/shared-session/${this.state.shareReference}`}/>

                                        <div className={'mt-10'}>
                                            <div className="btn-group btn-group-sm">
                                                <CopyToClipboard text={`${process.env.REACT_APP_SITE_HOST}/${this.context.user.locale}/shared-session/${this.state.shareReference}`} onCopy={this.toggleShareLinkCopied}>
                                                    <button type="button" className={btnClasses}>
                                                        <span className="text-light"><MdContentCopy/></span>
                                                        <span className="mobile-text ml-05">{this.state.shareLinkCopied ? t('Copied') : t('Copy')}</span>
                                                    </button>
                                                </CopyToClipboard>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </>
                        }
                    </div>
                    <div className="card-header text-center mobile-x-padding-5">
                        <button disabled={((!sessionEnabled || !thisDeviceSession) && !isUserPremium && isOwned)}
                                className="btn btn-primary" type="button" onClick={this.collapseWindows}>
                            {t('Windows')}
                            {this.state.windowsCollapsed
                                ? <span className="fw-500 ml-05"><MdKeyboardArrowDown/></span>
                                : <span className="fw-500 ml-05"><MdKeyboardArrowUp/></span>
                            }
                        </button>
                        <div className={collapseClasses}>
                            {this.props.session.windows.length > 0 ?
                                <SessionWindowList
                                    session={this.props.session}
                                    windows={this.props.session.windows}
                                    getSessions={this.props.getSessions}
                                    updateSessions={this.props.updateSessions}
                                /> : <NoneWindows/>
                            }
                        </div>
                    </div>
                    <AddCollaboratorModal
                        session={this.props.session}
                        show={this.state.showAddCollaboratorModal}
                        toggleModal={this.toggleAddCollaboratorModal}
                    />
                    <CollaboratedUsersModal
                        getSessions={this.props.getSessions}
                        updateSessions={this.props.updateSessions}
                        session={this.props.session}
                        show={this.state.showCollaboratedUsersModal}
                        toggleModal={this.toggleCollaboratedUsersModal}
                        users={this.state.collaboratedUsers}
                        setUsers={this.setCollaboratedUsers}
                    />
                </div>
        )
    }
}

export default SessionCard