import React from "react";
import {NotBlank} from "../../../infrastructure/FormBuilder/validators";
import FormBuilder from "../../../infrastructure/FormBuilder/FormBuilder";
import AppLoader from "../../../elements/Loader/AppLoader";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/apiClient";

class SessionEditForm extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            form: {
                isValid: true,
                children: {
                    name: {
                        label: 'Session name',
                        type: 'text',
                        value: props.session.name,
                        validators: [
                            new NotBlank('Session name cannot be empty')
                        ]
                    }
                }
            }
        }
    }

    onSave = (e) => {
        e.preventDefault();

        if (this.state.form.isValid) {
            this.setState({isLoading: true});

            apiClient(this.context.token, this.context.deviceId).patch(`/sessions/${this.props.session.id}`, {name: this.state.form.children.name.value})
                .then((res) => {
                    const sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            sessions[i] = res.data;

                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                })
        }
    }

    render() {
        return (
            this.state.isLoading
                ? <AppLoader/>
                : <FormBuilder form={this.state.form} onSave={this.onSave} onCancel={this.props.toggleEdit} />
        )
    }
}

export default SessionEditForm;