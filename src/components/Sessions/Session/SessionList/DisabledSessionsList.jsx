import React from "react";
import SessionCard from "../SessionCard";

class DisabledSessionsList extends React.Component {
    render() {
        return (
            this.props.sessions.map(session => (
                <div className="col-xl-6 col-lg-12 col-md-12 col-12" key={`${session.id}-session`}>
                    <SessionCard
                        getSessionsWithNonCollapsedWindows={this.props.getSessionsWithNonCollapsedWindows}
                        updateSessionsWithNonCollapsedWindows={this.props.updateSessionsWithNonCollapsedWindows}
                        getSessions={this.props.getSessions}
                        updateSessions={this.props.updateSessions}
                        session={session}
                    />
                </div>
            ))
        )
    }
}

export default DisabledSessionsList;