import React from "react";
import {SortableContainer, SortableElement} from "react-sortable-hoc";
import SessionCard from "../SessionCard";
import {Row} from "shards-react";
import {makeArrayOfIds} from "../../../../infrastructure/helpers";
import arrayMove from "array-move";
import {AppContext} from "../../../App/AppContext";
import AppLoader from "../../../../elements/Loader/AppLoader";
import apiClient from "../../../../infrastructure/Api/apiClient";

class SortableSessionsList extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {isLoading: false};
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        if (oldIndex !== newIndex) {
            const newOrder = arrayMove(this.props.sessions, oldIndex, newIndex);

            this.setState({isLoading: true});
            apiClient(this.context.token, this.context.deviceId).put(`/sessions/order`, {'sessions': makeArrayOfIds(newOrder)}).then(() => {
                let sessions = this.props.getSessions();

                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i]['is_enabled']) {
                        sessions.splice(i, 1);
                        i--;
                    }
                }

                const sessionsWithNewOrder = [...newOrder, ...sessions];
                this.props.updateSessions(sessionsWithNewOrder);
                this.setState({isLoading: false});
            })
        }
    };

    render() {
        const SortableItem = SortableElement(({session}) => {
                return (
                    <div className="col-xl-6 col-lg-12 col-md-12 col-12">
                        <SessionCard
                            getSessionsWithNonCollapsedWindows={this.props.getSessionsWithNonCollapsedWindows}
                            updateSessionsWithNonCollapsedWindows={this.props.updateSessionsWithNonCollapsedWindows}
                            getSessions={this.props.getSessions}
                            updateSessions={this.props.updateSessions}
                            session={session}
                        />
                    </div>
                )
            }
        );

        const SortableList = SortableContainer(({sessions}) => {
            return (
                <Row>
                    {
                        sessions.map((session, index) => (
                            <SortableItem key={`session-${session.id}`} index={index} session={session}/>
                        ))
                    }
                </Row>
            );
        });

        return this.state.isLoading ? <AppLoader/> :
            <SortableList axis="xy" pressDelay={150} sessions={this.props.sessions} onSortEnd={this.onSortEnd}/>
    }
}

export default SortableSessionsList;