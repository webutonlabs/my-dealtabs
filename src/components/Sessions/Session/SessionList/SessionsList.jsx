import React from "react";
import SortableSessionsList from "./SortableSessionsList";
import {Row} from "shards-react";
import DisabledSessionsList from "./DisabledSessionsList";
import {filterByEnableStatus, isUserPremium} from "../../../../infrastructure/helpers";
import {AppContext} from "../../../App/AppContext";

class SessionsList extends React.Component {
    static contextType = AppContext;

    render() {
        let sortableSessions;
        let notSortableSessions = [];

        if (isUserPremium(this.context.user)) {
            sortableSessions = this.props.sessions;
        } else {
            let [enabledSessions, disabledSessions] = filterByEnableStatus(this.props.sessions);

            let notThisDeviceSessions = [];
            let enabledAndThisDeviceSessions = [];
            enabledSessions.forEach(session => {
                if (session['device_id'] !== this.context.deviceId) {
                    notThisDeviceSessions.push(session);
                } else {
                    enabledAndThisDeviceSessions.push(session);
                }
            })

            sortableSessions = enabledAndThisDeviceSessions;
            notSortableSessions = [...disabledSessions, ...notThisDeviceSessions];
        }

        return (
            <>
                <SortableSessionsList
                    getSessionsWithNonCollapsedWindows={this.props.getSessionsWithNonCollapsedWindows}
                    updateSessionsWithNonCollapsedWindows={this.props.updateSessionsWithNonCollapsedWindows}
                    sessions={sortableSessions}
                    getSessions={this.props.getSessions}
                    updateSessions={this.props.updateSessions}
                />
                {notSortableSessions.length > 0 &&
                    <>
                        <Row>
                            <DisabledSessionsList
                                getSessionsWithNonCollapsedWindows={this.props.getSessionsWithNonCollapsedWindows}
                                updateSessionsWithNonCollapsedWindows={this.props.updateSessionsWithNonCollapsedWindows}
                                sessions={notSortableSessions}
                                getSessions={this.props.getSessions}
                                updateSessions={this.props.updateSessions}
                            />
                        </Row>
                    </>
                }
            </>
        )
    }
}

export default SessionsList;