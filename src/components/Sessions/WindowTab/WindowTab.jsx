import React from "react";
import {sliceString} from "../../../infrastructure/helpers";
import {MdClear, MdCreate, MdContentCopy} from 'react-icons/md'
import WindowTabEditForm from "./WindowTabEditForm";
import AppLoader from "../../../elements/Loader/AppLoader";
import {AppContext} from "../../App/AppContext";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import apiClient from "../../../infrastructure/Api/apiClient";
import TabIcon from "../../../elements/TabIcon";

class WindowTab extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            onEdit: false,
            tabCopied: false
        }
    }

    deleteWindowTab = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).delete(`/window-tabs/${this.props.tab.id}`)
            .then(() => {
                this.setState({isLoading: false})

                const sessions = this.props.getSessions();

                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        for (let j = 0; j < sessions[i].windows.length; j++) {
                            if (sessions[i].windows[j].id === this.props.window.id) {
                                for (let k = 0; k < sessions[i].windows[j].tabs.length; k++) {
                                    if (sessions[i].windows[j].tabs[k].id === this.props.tab.id) {
                                        sessions[i].windows[j].tabs.splice(k, 1);

                                        break;
                                    }
                                }

                                break;
                            }
                        }
                        break;
                    }
                }

                this.props.updateSessions(sessions);
            })
    }

    toggleEdit = () => {
        this.setState({onEdit: !this.state.onEdit});
    }

    toggleTabCopied = () => {
        this.setState({tabCopied: true});

        setTimeout(() => {
            this.setState({tabCopied: false})
        }, 2000)
    }

    render() {
        const t = this.context.t;

        const tabName = this.props.tab.name === null ? this.props.tab.url : this.props.tab.name;
        const isEnabled = this.props.tab['is_enabled'] && this.props.window['is_enabled'];
        const linkClasses = `ml-05 mobile-text ${!isEnabled ? 'text-dark' : ''}`;
        const btnClasses = `btn btn-white mobile-text ${!isEnabled ? 'disabled' : ''}`;

        return (
            this.state.isLoading ? <AppLoader/> :
                <>
                    <div className="container-fluid">
                        <div className="row mt-10 text-center blue-on-hover">
                            <div className="col-xl-12 col-lg-12 col-md-12 col-12 mt-10">
                                <TabIcon src={this.props.tab.icon}/>
                                <a className={linkClasses} target="_blank"
                                   href={isEnabled ? this.props.tab.url : false}>{sliceString(tabName, 100)}</a>
                            </div>
                            {!this.state.onEdit &&
                            <div className="col-xl-12 col-lg-12 col-md-12 col-12 mt-10 mb-10">
                                <div style={{paddingBottom: '5px'}} className="btn-group btn-group-sm">
                                    {isEnabled &&
                                    <CopyToClipboard text={this.props.tab.url} onCopy={this.toggleTabCopied}>
                                        <button type="button" className={btnClasses}>
                                           <span className="text-light">
                                                <MdContentCopy/>
                                           </span>
                                            <span className="mobile-text ml-05">
                                                {this.state.tabCopied ? t('Copied') : t('Copy')}
                                            </span>
                                        </button>
                                    </CopyToClipboard>
                                    }
                                    <button onClick={isEnabled ? this.toggleEdit : () => {
                                    }} type="button" className={btnClasses}>
                                        <span className="text-light">
                                            <MdCreate/>
                                        </span>
                                        <span className="mobile-text ml-05">{t('Edit')}</span>
                                    </button>
                                    <button onClick={this.deleteWindowTab} type="button" className="btn btn-white">
                                        <span className="text-danger">
                                            <MdClear/>
                                        </span>
                                        <span className="mobile-text ml-05">{t('Delete')}</span>
                                    </button>
                                </div>
                            </div>
                            }
                        </div>
                    </div>
                    {this.state.onEdit &&
                    <div className="container-fluid">
                        <div className="row mt-10">
                            <div className="col-xl-12 col-lg-12 col-md-12 col-12">
                                <WindowTabEditForm
                                    tab={this.props.tab}
                                    session={this.props.session}
                                    window={this.props.window}
                                    toggleEdit={this.toggleEdit}
                                    getSessions={this.props.getSessions}
                                    updateSessions={this.props.updateSessions}
                                />
                            </div>
                        </div>
                    </div>
                    }
                </>
        )
    }
}

export default WindowTab;