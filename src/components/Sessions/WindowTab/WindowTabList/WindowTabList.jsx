import React from "react";
import SortableWindowTabList from "./SortableWindowTabList";
import DisabledWindowTabList from "./DisabledWindowTabList";
import NotificationAlert from "../../../../elements/NotificationAlert";
import {AppContext} from "../../../App/AppContext";
import {filterByEnableStatus} from "../../../../infrastructure/helpers";

class WindowTabList extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        const [enabledTabs, disabledTabs] = filterByEnableStatus(this.props.tabs);

        return (
            <>
                {disabledTabs.length > 0 &&
                    <div className="mt-10">
                        <NotificationAlert type="warning">
                            <span className="fw-500 mobile-text">{t('notification-disabled-tabs-1')}</span>
                        </NotificationAlert>
                    </div>
                }
                <SortableWindowTabList
                    tabs={enabledTabs}
                    window={this.props.window}
                    session={this.props.session}
                    getSessions={this.props.getSessions}
                    updateSessions={this.props.updateSessions}
                />
                <DisabledWindowTabList
                    tabs={disabledTabs}
                    window={this.props.window}
                    session={this.props.session}
                    getSessions={this.props.getSessions}
                    updateSessions={this.props.updateSessions}
                />
            </>
        )
    }
}

export default WindowTabList;