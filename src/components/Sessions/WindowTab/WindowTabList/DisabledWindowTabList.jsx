import React from "react";
import WindowTab from "../WindowTab";

class DisabledWindowTabList extends React.Component {
    render() {
        return (
            this.props.tabs.map(tab => (
                <ul key={tab.id} className="list-group list-group-small list-group-flush">
                    <WindowTab
                        getSessions={this.props.getSessions}
                        updateSessions={this.props.updateSessions}
                        session={this.props.session}
                        window={this.props.window}
                        tab={tab}
                    />
                </ul>
            ))
        )
    }
}

export default DisabledWindowTabList;