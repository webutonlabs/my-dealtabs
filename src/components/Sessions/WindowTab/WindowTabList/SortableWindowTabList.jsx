import React from "react";
import {SortableContainer, SortableElement} from "react-sortable-hoc";
import WindowTab from "../WindowTab";
import {filterByEnableStatus, makeArrayOfIds} from "../../../../infrastructure/helpers";
import arrayMove from "array-move";
import {AppContext} from "../../../App/AppContext";
import AppLoader from "../../../../elements/Loader/AppLoader";
import apiClient from "../../../../infrastructure/Api/apiClient";

class SortableWindowTabList extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {isLoading: false};
    }

    onSortEnd = ({oldIndex, newIndex}) => {
        if (oldIndex !== newIndex) {
            const newOrder = arrayMove(this.props.tabs, oldIndex, newIndex);

            this.setState({isLoading: true});
            apiClient(this.context.token, this.context.deviceId).patch(`/session-windows/${this.props.window.id}/order-tabs`, {'tabs': makeArrayOfIds(newOrder)})
                .then(() => {
                    let sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            for (let j = 0; j < sessions[i].windows.length; j++) {
                                if (sessions[i].windows[j].id === this.props.window.id) {
                                    const [enabledTabs, disabledTabs] = filterByEnableStatus(sessions[i].windows[j].tabs);
                                    sessions[i].windows[j].tabs = [...newOrder, ...disabledTabs];

                                    break;
                                }
                            }

                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                    this.setState({isLoading: false});
                })
        }
    };

    render() {
        const SortableItem = SortableElement(({tab}) => {
                return (
                    <>
                        <WindowTab
                            getSessions={this.props.getSessions}
                            updateSessions={this.props.updateSessions}
                            session={this.props.session}
                            window={this.props.window}
                            tab={tab}
                        />
                    </>
                )
            }
        );

        const SortableList = SortableContainer(({tabs}) => {
            return (
                <div className="row">
                    {tabs.map((tab, index) => (
                        <SortableItem key={`item-${tab.id}`} index={index} tab={tab}/>
                    ))}
                </div>
            );
        });

        return this.state.isLoading ? <AppLoader /> :
            <SortableList pressDelay={200} tabs={this.props.tabs} onSortEnd={this.onSortEnd}/>;
    }
}

export default SortableWindowTabList;