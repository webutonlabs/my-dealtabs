import React from "react";
import {NotBlank} from "../../../infrastructure/FormBuilder/validators";
import FormBuilder from "../../../infrastructure/FormBuilder/FormBuilder";
import AppLoader from "../../../elements/Loader/AppLoader";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/apiClient";

class WindowTabEditForm extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                isValid: true,
                children: {
                    name: {
                        label: 'Tab name',
                        type: 'text',
                        value: props.tab.name,
                        validators: [
                            new NotBlank('Tab name cannot be empty')
                        ]
                    },
                    url: {
                        label: 'Tab url',
                        type: 'text',
                        value: props.tab.url,
                        validators: [
                            new NotBlank('Tab url cannot be empty')
                        ]
                    },
                    session: {
                        type: 'select',
                        label: 'Session',
                        data: [],
                        value: props.session.id.toString(),
                        translate: false
                    },
                    window: {
                        type: 'select',
                        label: 'Session window',
                        data: [],
                        value: props.window.id.toString(),
                        translate: false
                    },
                }
            }
        }
    }

    componentDidMount() {
        this.setState({isLoading: true});

        Promise.all([
            apiClient(this.context.token, this.context.deviceId).get(`/sessions?per_page=999`),
            apiClient(this.context.token, this.context.deviceId).get(`/sessions/${this.props.session.id}`)
        ]).then(([sessionsResponse, sessionResponse]) => {
            let form = this.state.form;
            form.children.session.data = sessionsResponse.data;
            form.children.window.data = sessionResponse.data.windows;

            this.setState({form: form, isLoading: false})
        })
    }

    onSave = (e) => {
        const client = apiClient(this.context.token, this.context.deviceId);

        e.preventDefault();

        if (this.state.form.isValid) {
            this.setState({isLoading: true});

            const data = {
                name: this.state.form.children.name.value,
                url: this.state.form.children.url.value
            }

            if (Number(this.state.form.children.window.value) !== this.props.window.id) {
                data.window = this.state.form.children.window.value;
            }

            client.patch(`/window-tabs/${this.props.tab.id}`, data).then((res) => {
                if (this.props.window.id.toString() !== this.state.form.children.window.value) {
                    window.location.href = `/sessions?toast=success&message=${this.context.t('changes-saved')}`
                } else {
                    const sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            for (let j = 0; j < sessions[i].windows.length; j++) {
                                if (sessions[i].windows[j].id === this.props.window.id) {
                                    for (let k = 0; k < sessions[i].windows[j].tabs.length; k++) {
                                        if (sessions[i].windows[j].tabs[k].id === this.props.tab.id) {
                                            sessions[i].windows[j].tabs[k] = res.data;

                                            break;
                                        }
                                    }

                                    break;
                                }
                            }
                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                }
            })
        }
    }

    onChangeSession = (e) => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).get(`/sessions/${e.target.value}`).then(sessionResponse => {
            let form = this.state.form;
            form.children.window.data = sessionResponse.data.windows;
            if (sessionResponse.data.windows.length > 0) {
                form.children.window.value = sessionResponse.data.windows[0].id;
            }

            this.setState({form: form, isLoading: false})
        })
    }

    render() {
        let form = this.state.form;
        form.children.session.changeCallback = this.onChangeSession;

        return (
            this.state.isLoading
                    ? <AppLoader/>
                    : <FormBuilder form={form} onSave={this.onSave} onCancel={this.props.toggleEdit} />
        )
    }
}

export default WindowTabEditForm;