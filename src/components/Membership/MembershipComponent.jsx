import React from "react";
import AppLoader from "../../elements/Loader/AppLoader";
import MainContent from "../../elements/MainContent";
import PageTitle from "../../elements/PageTitle";
import {AppContext} from "../App/AppContext";
import MembershipPlans from "./MembershipPlans/MembershipPlans";
import {successToast, toastFromQuery} from "../../infrastructure/toast";
import NotificationAlert from "../../elements/NotificationAlert";
import {Constants, Dispatcher, Store} from "../../flux";

class MembershipComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        const urlSearchParams = new URLSearchParams(window.location.search);

        this.state = {
            isLoading: false,
            redirectedFromProcessedPayment: urlSearchParams.has('result') && urlSearchParams.get('result') === 'order_processed'
        };
    }

    componentDidMount() {
        if (Store.getMenuState() === true) {
            Dispatcher.dispatch({actionType: Constants.TOGGLE_SIDEBAR});
        }

        toastFromQuery();

        const urlSearchParams = new URLSearchParams(window.location.search);
        if (urlSearchParams.has('data') && urlSearchParams.has('signature')) { // Fondy redirect
            this.setState({redirectedFromProcessedPayment: true});

            setTimeout(() => {
                window.location.href = '/membership';
            }, 5000);
        }
    }

    render() {
        const t = this.context.t;

        return (
            this.state.isLoading ? <AppLoader/> :
                <>
                    {this.state.redirectedFromProcessedPayment &&
                        <NotificationAlert type="accent">
                            <span className="fw-500 mobile-text">{t('subscription-will-be-updated-soon')} </span>
                        </NotificationAlert>
                    }
                    <MainContent>
                        <PageTitle title={t('Manage membership')} subtitle={t('Subscription plan')}/>
                        <div className={'container-fluid'}>
                            <MembershipPlans/>
                            <div className="row">
                                <div className="col-12">
                                    <div className="card card-small mb-4">
                                        <div className="card-header border-bottom">
                                            <h6 className="m-0">{t('Membership plans')}</h6>
                                        </div>
                                        <div className="card-body p-0 pb-3 text-left">
                                            <table className="table mb-0">
                                                <thead className="bg-light">
                                                <tr>
                                                    <th scope="col" className="border-0"/>
                                                    <th scope="col" className="border-0">Basic</th>
                                                    <th scope="col" className="border-0">Premium</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{t('Extensions')}</td>
                                                    <td>Google Chrome, Mozilla Firefox, Opera</td>
                                                    <td>Google Chrome, Mozilla Firefox, Opera</td>
                                                </tr>
                                                <tr>
                                                    <td>{t('Session limit')}</td>
                                                    <td>5</td>
                                                    <td>{t('Unlimited')}</td>
                                                </tr>
                                                <tr>
                                                    <td>{t('Windows per session limit')}</td>
                                                    <td>2</td>
                                                    <td>{t('Unlimited')}</td>
                                                </tr>
                                                <tr>
                                                    <td>{t('Tabs per window limit')}</td>
                                                    <td>20</td>
                                                    <td>{t('Unlimited')}</td>
                                                </tr>
                                                <tr>
                                                    <td>{t('quick-tabs-limit')}</td>
                                                    <td>15</td>
                                                    <td>{t('Unlimited')}</td>
                                                </tr>
                                                <tr>
                                                    <td>{t('Device sync')}</td>
                                                    <td>{t('Disabled')}</td>
                                                    <td>{t('Enabled')}</td>
                                                </tr>
                                                <tr>
                                                    <td>{t('Save shared session')}</td>
                                                    <td>{t('Disabled')}</td>
                                                    <td>{t('Enabled')}</td>
                                                </tr>
                                                <tr>
                                                    <td>{t('collaboration')}</td>
                                                    <td>{t('Partial')}</td>
                                                    <td>{t('Enabled')}</td>
                                                </tr>
                                                <tr>
                                                    <td>{t('live-sync')}</td>
                                                    <td>{t('Disabled')}</td>
                                                    <td>{t('Enabled')}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </MainContent>
                </>
        )
    }
}

export default MembershipComponent;