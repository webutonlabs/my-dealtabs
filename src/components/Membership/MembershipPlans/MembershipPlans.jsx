import React from "react";
import BasicPlan from "./BasicPlan";
import {AppContext} from "../../App/AppContext";
import PremiumPlan from "./PremiumPlan";

class MembershipPlans extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        return (
            <div className={'row'}>
                <div className={'container-fluid'}>
                    <div className={'card-deck'}>
                        <BasicPlan
                            title={t('Basic')}
                            subtitle={t('Free')}
                        />
                        <PremiumPlan
                            title={t('Premium')}
                            subtitle={t('starting-from') + ' 2.29$'}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default MembershipPlans;