import React from "react";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/apiClient";
import {errorToast, infoToast} from "../../../infrastructure/toast";
import {resolveUserMembership} from "./helpers";
import SmallList from "../../../elements/List/SmallList";
import SmallListElement from "../../../elements/List/SmallListElement";

class PremiumPlan extends React.Component {
    static contextType = AppContext;

    unsubscribe = () => {
        apiClient(this.context.token, this.context.deviceId).patch('/user/cancel-subscription').then(res => {
            infoToast(this.context.t('subscription-cancelled'));

            setTimeout(() => {
                window.location.reload();
            }, 5000)
        }).catch(err => {
            if (err.response.data.resource === 'fondy') {
                errorToast(this.context.t('fondy-not-support-cancel-subscription'), 10000)
            } else {
                errorToast(err.response.data.message, 10000)
            }
        });
    }

    subscribe = () => {
        window.location.href = process.env.REACT_APP_BILLING_SERVICE_HOST + `/${this.context.user.locale}/dealtabs/payment?email=${this.context.user.email}`
    }

    render() {
        const t = this.context.t;
        const {isPremium, isGift, isRecurring, expiresDate, isCancelled, isExpiring} = resolveUserMembership(this.context.user);

        return (
            <div className="card text-center">
                <div className={'card-header border-bottom'}>
                    <div className="mb-3 mx-auto">
                        <img className="rounded-circle" src={require('../../../assets/logo.png')}
                             alt="User Avatar" width="110"/></div>
                    <h4 className="mb-0">{this.props.title}</h4>
                    <span className="text-muted d-block mb-2">{this.props.subtitle}</span>
                </div>
                <div className="card-body border-bottom">
                    {isPremium && !isGift && !isCancelled &&
                    <div className="card-text text-success">
                        {t('your-current-plan')}
                    </div>
                    }
                    {(!isPremium || isGift || isCancelled) &&
                        <div className="card-text">
                            {t('none-restrictions-will-be-applied')}
                        </div>
                    }
                    <SmallList>
                        <SmallListElement
                            name={t('Session limit')}
                            value={t('Unlimited')}
                        />
                        <SmallListElement
                            name={t('Windows per session limit')}
                            value={t('Unlimited')}
                        />
                        <SmallListElement
                            name={t('Tabs per window limit')}
                            value={t('Unlimited')}
                        />
                        <SmallListElement
                            name={t('quick-tabs-limit')}
                            value={t('Unlimited')}
                        />
                        <SmallListElement
                            name={t('Device sync')}
                            value={t('Enabled')}
                        />
                        <SmallListElement
                            name={t('collaboration')}
                            value={t('Enabled')}
                        />
                        <SmallListElement
                            name={t('live-sync')}
                            value={t('Enabled')}
                        />
                        <SmallListElement
                            name={t('Save shared session')}
                            value={t('Enabled')}
                        />
                    </SmallList>
                </div>
                <div className="card-footer">
                    {(isPremium && !isCancelled && !isGift && !isExpiring) &&
                    <>
                        <h6 className={'card-text'}>{t('Premium expires')}: {expiresDate}</h6>
                    </>
                    }
                    {(isPremium && !isCancelled && !isGift && !isExpiring && isRecurring) &&
                    <>
                        <h6 className={'card-text'}>{t('next-payment')}: ~ {expiresDate}</h6>
                    </>
                    }
                    {(isPremium && !isCancelled && !isGift && isRecurring) &&
                    <div className={'mt-2'}>
                        <button onClick={this.unsubscribe} className={'btn btn-danger'}>{t('cancel-subscription')}</button>
                    </div>
                    }
                    {(!isPremium || isCancelled || isGift || !isRecurring) &&
                    <div className={'mt-2'}>
                        <button onClick={this.subscribe} className={'btn btn-success'}>{t('subscribe')}</button>
                    </div>
                    }
                </div>
            </div>
        )
    }
}

export default PremiumPlan;