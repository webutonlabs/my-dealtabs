import React from "react";
import {AppContext} from "../../App/AppContext";
import {resolveUserMembership} from "./helpers";
import SmallList from "../../../elements/List/SmallList";
import SmallListElement from "../../../elements/List/SmallListElement";

class BasicPlan extends React.Component {
    static contextType = AppContext;

    unsubscribe = () => {
        window.location.href = process.env.REACT_APP_API_HOST + '/subscription/invoke-unsubscription'
    }

    render() {
        const t = this.context.t;
        const {isPremium, isGift, isRecurring, expiresDate, isExpiring} = resolveUserMembership(this.context.user);

        return (
            <div className="card text-center">
                <div className={'card-header border-bottom'}>
                    <div className="mb-3 mx-auto">
                        <img className="rounded-circle" src={require('../../../assets/logo_grey.png')}
                             alt="User Avatar" width="110"/></div>
                    <h4 className="mb-0">{this.props.title}</h4>
                    <span className="text-muted d-block mb-2">{this.props.subtitle}</span>
                </div>
                <div className="card-body border-bottom">
                    {(!isPremium || isGift || isExpiring) &&
                    <p className="card-text text-success">
                        {t('your-current-plan')}
                    </p>
                    }
                    <p className="card-text">
                        {t('some-restrictions-will-be-applied')}
                    </p>
                    <SmallList>
                        <SmallListElement
                            name={t('Session limit')}
                            value={5}
                        />
                        <SmallListElement
                            name={t('Windows per session limit')}
                            value={2}
                        />
                        <SmallListElement
                            name={t('Tabs per window limit')}
                            value={20}
                        />
                        <SmallListElement
                            name={t('quick-tabs-limit')}
                            value={15}
                        />
                        <SmallListElement
                            name={t('Device sync')}
                            value={t('Disabled')}
                        />
                        <SmallListElement
                            name={t('collaboration')}
                            value={t('Partial')}
                        />
                        <SmallListElement
                            name={t('live-sync')}
                            value={t('Disabled')}
                        />
                        <SmallListElement
                            name={t('Save shared session')}
                            value={t('Disabled')}
                        />
                    </SmallList>
                </div>
                <div className="card-footer">
                    {(isPremium && expiresDate !== null) &&
                    <>
                        <h6 className={'card-text'}>{t('Premium expires')}: {expiresDate}</h6>
                    </>
                    }
                    {!isPremium &&
                        <p className={'card-text text-secondary'}>
                            {t('you-are-on-a-free-plan')}
                        </p>
                    }
                    {isPremium && isGift &&
                        <p className={'card-text text-secondary'}>
                            {t('you-are-on-gift-sub')}
                        </p>
                    }
                </div>
            </div>
        )
    }
}

export default BasicPlan;