import {isUserPremium} from "../../../infrastructure/helpers";

const resolveUserMembership = (user) => {
    const isPremium = isUserPremium(user);
    const isExpiringSub = user.membership.subscriptionType === 'expiring';
    const isGiftSub = user.membership.subscriptionType === 'gift';
    const isRecurringSub = ['liqpay_recurring_monthly', 'liqpay_recurring_yearly', 'fondy_recurring_monthly', 'fondy_recurring_yearly'].includes(user.membership.subscriptionType);
    const isCancelled = user.membership.isCancelled;

    let expiresDate = null;
    if (isPremium) {
        expiresDate = new Date(user.membership.subscriptionExpires);
        expiresDate = `${(expiresDate.toLocaleDateString(undefined, {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        }))}`
    }

    return {
        isPremium: isPremium,
        isExpiring: isExpiringSub,
        isGift: isGiftSub,
        isRecurring: isRecurringSub,
        isCancelled: isCancelled,
        expiresDate: expiresDate
    }
}

export {resolveUserMembership}