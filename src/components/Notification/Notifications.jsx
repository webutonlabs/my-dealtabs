import React from "react";
import WarningAlert from "../../elements/Alert/WarningAlert";
import DangerAlert from "../../elements/Alert/DangerAlert";
import {FaInfo} from "react-icons/all";
import {toast} from "react-toastify";
import apiClient from "../../infrastructure/Api/apiClient";
import {AppContext} from "../App/AppContext";

class Notifications extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            alertNotifications: [],
            warningNotifications: []
        }
    }

    readTextNotification = (id) => {
        apiClient(this.context.token, this.context.deviceId).patch(`/notifications/text-notifications/${id}/read`).then(() => {
            //
        });
    }

    componentDidMount() {
        apiClient(this.context.token, this.context.deviceId).get('/notifications?scopes=text,warning,alert').then(res => {
            const notifications = res.data;

            this.setState({
                warningNotifications: notifications.warning,
                alertNotifications: notifications.alert
            });

            let timeOut = 0;
            notifications.text.forEach(notification => {
                const options = {
                    position: 'bottom-right',
                    hideProgressBar: true,
                    draggable: false,
                    autoClose: false,
                    closeOnClick: false,
                    onClose: () => {this.readTextNotification(notification.id)},
                    toastId: notification.id
                };

                const body = (
                    <>
                        <FaInfo />
                        <span style={{marginLeft: '5px', fontSize: '16px', fontWeight: '500'}}>{notification.content}</span>
                    </>
                );

                setTimeout(() => {toast.info(body, options)}, timeOut);
                timeOut += 1000;
            });
        })
    }

    render() {
        return (
            <>
                {this.state.warningNotifications.map(notification => {
                    return <WarningAlert key={notification.id} content={notification.content}/>
                })}
                {this.state.alertNotifications.map(notification => {
                    return <DangerAlert key={notification.id} content={notification.content}/>
                })}
            </>
        )
    }
}

export default Notifications;