import React from "react";
import ArchivedTab from "./ArchivedTab";

class ArchivedTabsList extends React.Component {
    render() {
        return (
            <>
                {this.props.tabs.map(tab => {
                    return <ArchivedTab
                        tab={tab}
                        key={tab.id}
                        getTabs={this.props.getTabs}
                        setTabs={this.props.setTabs}
                    />
                })}
            </>
        )
    }
}

export default ArchivedTabsList;