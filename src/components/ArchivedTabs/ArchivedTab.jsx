import React from "react";
import {sliceString} from "../../infrastructure/helpers";
import {FaTrash} from "react-icons/all";
import AppLoader from "../../elements/Loader/AppLoader";
import apiClient from "../../infrastructure/Api/apiClient";
import {AppContext} from "../App/AppContext";
import TabIcon from "../../elements/TabIcon";

class ArchivedTab extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        }
    }

    deleteTab = () => {
        this.setState({isLoading: true})

        apiClient(this.context.token, this.context.deviceId).delete(`/archived-tabs/${this.props.tab.id}`).then(() => {
            let tabs = this.props.getTabs();

            for (let i = 0; i < tabs.length; i++) {
                if (tabs[i].id === this.props.tab.id) {
                    tabs.splice(i, 1);
                    break;
                }
            }

            this.setState({isLoading: false})
            this.props.setTabs(tabs);
        })
    }

    render() {
        return (
            this.state.isLoading ? <AppLoader /> :
                <div className={'col-lg-6 col-md-6 col-12 mt-3'}>
                    <div className={'card shadow'}>
                        <div className={'card-header align-items-center d-flex justify-content-between'} style={{borderBottom: "1px solid rgba(0,0,0,.2)"}}>
                            <div>
                                <TabIcon src={this.props.tab.icon}/>
                                <span className={'ml-05'}>
                                {sliceString(this.props.tab.name)}
                            </span>
                            </div>
                            <div>
                                <button onClick={this.deleteTab} className={'btn btn-sm btn-danger'}>
                                    <FaTrash />
                                </button>
                            </div>
                        </div>
                        <div className={'card-body p-3'}>
                            <a href={this.props.tab.url}>
                                {sliceString(this.props.tab.url, 65)}
                            </a>
                        </div>
                    </div>
                </div>
        )
    }
}

export default ArchivedTab;