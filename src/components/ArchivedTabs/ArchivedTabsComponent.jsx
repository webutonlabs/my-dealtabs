import React from "react";
import {Constants, Dispatcher, Store} from "../../flux";
import {AppContext} from "../App/AppContext";
import AppLoader from "../../elements/Loader/AppLoader";
import MainContent from "../../elements/MainContent";
import PageTitle from "../../elements/PageTitle";
import apiClient from "../../infrastructure/Api/apiClient";
import Pagination from "../Sessions/Pagination";
import ArchivedTabsList from "./ArchivedTabsList";
import NotificationAlert from "../../elements/NotificationAlert";

class ArchivedTabsComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            tabs: [],
            page: 1,
            perPage: 20,
            nextPage: null
        }
    }

    componentDidMount() {
        document.title = this.context.t('title-my-dealtabs-archived-tabs');

        if (Store.getMenuState() === true) {
            Dispatcher.dispatch({actionType: Constants.TOGGLE_SIDEBAR});
        }

        this.fetchTabs();
    }

    fetchTabs = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).get(`/archived-tabs?&per_page=${this.state.perPage}&page=${this.state.page}`)
            .then(res => {
                let tabs = [...this.state.tabs, ...res.data];

                this.setState({
                    isLoading: false,
                    page: this.state.page + 1,
                    nextPage: res.headers['x-pagination-next-page'] === 'null' ? null : Number(res.headers['x-pagination-next-page']),
                    tabs: tabs
                });

                if (this.state.page > 2) {
                    window.scrollTo(0, document.body.scrollHeight);
                }
            })
    }

    setTabs = (tabs) => {
        this.setState({tabs: tabs});
    }

    getTabs = () => {
        return this.state.tabs;
    }

    loadMore = () => {
        if (null !== this.state.nextPage) {
            this.fetchTabs();
        }
    }

    render() {
        return (
            this.state.isLoading ? <AppLoader/> :
                <>
                    <NotificationAlert type="warning">
                        <span className="fw-500 mobile-text">{this.context.t('archived-tabs-warning-1')} </span>
                    </NotificationAlert>
                    <MainContent>
                        <PageTitle title={this.context.t('archived-tabs')} subtitle={this.context.t('Overview')}/>
                        <div className="row mb-3">
                            {this.state.tabs.length === 0 &&
                                <div className={'col-12'}>
                                    <div className={'alert alert-success'}>{this.context.t('none-archived-tabs')}</div>
                                </div>
                            }
                            <ArchivedTabsList
                                tabs={this.state.tabs}
                                getTabs={this.getTabs}
                                setTabs={this.setTabs}
                            />
                        </div>
                        {null !== this.state.nextPage && <Pagination loadMore={this.loadMore}/>}
                    </MainContent>
                </>
        )
    }
}

export default ArchivedTabsComponent;