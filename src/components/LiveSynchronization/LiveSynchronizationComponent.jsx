import React from "react";
import {Constants, Dispatcher, Store} from "../../flux";
import {AppContext} from "../App/AppContext";
import AppLoader from "../../elements/Loader/AppLoader";
import MainContent from "../../elements/MainContent";
import PageTitle from "../../elements/PageTitle";
import apiClient from "../../infrastructure/Api/apiClient";
import SyncedDevicesList from "./SyncedDevicesList";
import {isUserPremium} from "../../infrastructure/helpers";
import NotificationAlert from "../../elements/NotificationAlert";

class LiveSynchronizationComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            syncedDevices: [],
            updateIntervalId: null
        }
    }

    componentDidMount() {
        document.title = this.context.t('title-my-dealtabs-live-sync');

        if (Store.getMenuState() === true) {
            Dispatcher.dispatch({actionType: Constants.TOGGLE_SIDEBAR});
        }

        if (isUserPremium(this.context.user)) {
            const updateIntervalId = setInterval(() => {
                apiClient(this.context.token, this.context.deviceId).get('/live-sync').then(res => {
                    const devices = res.data;

                    // moving current device to the end
                    for (let i = 0; i < devices.length; i++) {
                        if (devices[i].id === this.context.deviceId) {
                            devices.push(devices.splice(i, 1)[0]);
                            break;
                        }
                    }

                    for (const device of devices) {
                        device.tabs.sort((a, b) => {return a.orderIndex - b.orderIndex});
                    }

                    this.setState({syncedDevices: devices});
                });
            }, 1000);

            this.setState({updateIntervalId: updateIntervalId});
        }
    }

    componentWillUnmount() {
        if (null !== this.state.updateIntervalId) {
            clearInterval(this.state.updateIntervalId);
        }
    }

    render() {
        return (
            this.state.isLoading ? <AppLoader/> :
                <>
                    {!isUserPremium(this.context.user) &&
                    <>
                        <NotificationAlert type="danger">
                            <span className="fw-500 mobile-text">{this.context.t('notification-basic-membership-4')} </span>
                            <a href={'/membership'} className="text-white fw-500 mobile-text"><span>{this.context.t('notification-basic-membership-1')}</span></a>
                            <span className="fw-500 mobile-text"> {this.context.t('notification-basic-membership-2')}</span>
                        </NotificationAlert>
                    </>
                    }
                    {isUserPremium(this.context.user) &&
                        <MainContent>
                            <PageTitle title={this.context.t('live-sync')} subtitle={this.context.t('Overview')}/>
                            <div className="row mb-3">
                                {this.state.syncedDevices.length === 0 &&
                                    <div className={'col-12'}>
                                        <h5>{this.context.t('enable-live-sync')}</h5>
                                    </div>
                                }
                                <SyncedDevicesList devices={this.state.syncedDevices}/>
                            </div>
                        </MainContent>
                    }
                </>
        )
    }
}

export default LiveSynchronizationComponent;