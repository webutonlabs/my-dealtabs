import React from "react";
import SyncedDevice from "./SyncedDevice";

class SyncedDevicesList extends React.Component {
    render() {
        return (
            this.props.devices.map(device => {
                return <SyncedDevice key={device.id} device={device}/>
            })
        )
    }
}

export default SyncedDevicesList;