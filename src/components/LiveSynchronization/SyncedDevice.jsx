import React from "react";
import {sliceString} from "../../infrastructure/helpers";
import {FaTrash} from "react-icons/all";
import apiClient from "../../infrastructure/Api/apiClient";
import {successToast} from "../../infrastructure/toast";
import {AppContext} from "../App/AppContext";
import TabIcon from "../../elements/TabIcon";

class SyncedDevice extends React.Component {
    static contextType = AppContext;

    deleteDevice = () => {
        apiClient(this.context.token, this.context.deviceId).delete('/live-sync/delete-device').then(res => {
            successToast(this.context.t('successfully-deleted'));
        })
    }

    render() {
        return (
            <div className={'col-lg-6 col-12 mt-3'}>
                <div className={'card shadow'}>
                    <div className={'card-header text-center d-flex justify-content-between'} style={{borderBottom: '1px solid rgba(0,0,0,.125)'}}>
                        <div style={{width: '90%'}}>
                            {this.props.device.name}
                        </div>
                        <div>
                            <button onClick={this.deleteDevice} className={'btn btn-sm btn-danger'}>
                                <FaTrash />
                            </button>
                        </div>
                    </div>
                    <div className={'card-body p-2'}>
                        <ul className="list-group synced-device-tabs" style={{borderRadius: 0}}>
                            {this.props.device.tabs.map(syncedTab => {
                                return <li key={syncedTab.id} className={`list-group-item ${syncedTab['is_active'] ? 'active' : ''}`}>
                                    <span>
                                        <TabIcon src={syncedTab.icon}/>
                                    </span>
                                    <span className={'ml-2'}>
                                        <a href={syncedTab.url} target={'_blank'}>
                                            {sliceString(syncedTab.name, 80)}
                                        </a>
                                    </span>
                                </li>
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default SyncedDevice;