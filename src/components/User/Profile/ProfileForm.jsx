import React from "react";
import AppLoader from "../../../elements/Loader/AppLoader";
import {NotBlank} from "../../../infrastructure/FormBuilder/validators";
import FormBuilder from "../../../infrastructure/FormBuilder/FormBuilder";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/apiClient";

class ProfileForm extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                isValid: true,
                children: {
                    name: {
                        label: 'Name',
                        type: 'text',
                        value: this.props.user.name,
                        validators: [
                            new NotBlank('Name cannot be empty')
                        ]
                    },
                    surname: {
                        label: 'Surname',
                        type: 'text',
                        value: this.props.user.surname
                    },
                    locale: {
                        type: 'select',
                        label: 'Language',
                        data: [{id: 'ru', name: 'Russian'}, {id: 'en', name: 'English'}],
                        value: this.props.user.locale,
                        translate: true
                    }
                }
            }
        }
    }

    onSave = (e) => {
        e.preventDefault();

        if (this.state.form.isValid) {
            this.setState({isLoading: true});

            apiClient(this.context.token, this.context.deviceId).patch('/user', {
                name: this.state.form.children.name.value,
                surname: this.state.form.children.surname.value,
                locale: this.state.form.children.locale.value
            }).then((res) => {
                window.location.href = `/user?toast=success&message=${this.context.t('changes-saved')}`;
            })
        }
    }

    render() {
        return (
            this.state.isLoading
                ? <AppLoader/>
                : <FormBuilder form={this.state.form} onSave={this.onSave} />
        )
    }
}

export default ProfileForm;