import React from "react";
import AppLoader from "../../../elements/Loader/AppLoader";
import {Email} from "../../../infrastructure/FormBuilder/validators";
import FormBuilder from "../../../infrastructure/FormBuilder/FormBuilder";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/apiClient";
import {errorToast} from "../../../infrastructure/toast";

class ChangeEmailForm extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                isValid: true,
                children: {
                    email: {
                        label: 'Email',
                        type: 'text',
                        value: '',
                        validators: [
                            new Email()
                        ]
                    }
                }
            }
        }
    }

    onSave = (e) => {
        e.preventDefault();

        if (this.state.form.isValid) {
            this.setState({isLoading: true});

            apiClient(this.context.token, this.context.deviceId).patch('/user', {
                email: this.state.form.children.email.value,
            }).then((res) => {
                window.location.href = `/user?toast=success&message=${this.context.t('changes-saved')}`;
            }).catch(err => {
                errorToast(this.context.t('user-exists'));

                this.setState({isLoading: false})
            })
        }
    }

    render() {
        return (
            this.state.isLoading
                ? <AppLoader/>
                : <FormBuilder form={this.state.form} onSave={this.onSave} />
        )
    }
}

export default ChangeEmailForm;