import React from "react";
import AppLoader from "../../../elements/Loader/AppLoader";
import FormBuilder from "../../../infrastructure/FormBuilder/FormBuilder";
import {Length} from "../../../infrastructure/FormBuilder/validators";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/apiClient";

class ChangePasswordForm extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                isValid: true,
                children: {
                    password: {
                        label: 'Password',
                        type: 'password',
                        value: '',
                        validators: [
                            new Length(5, 'Password is too short')
                        ]
                    }
                }
            }
        }
    }

    onSave = (e) => {
        e.preventDefault();

        if (this.state.form.isValid) {
            this.setState({isLoading: true});

            apiClient(this.context.token, this.context.deviceId).patch('/user', {
                password: this.state.form.children.password.value,
            }).then(() => {
                window.location.href = `/user?toast=success&message=${this.context.t('changes-saved')}`;
            })
        }
    }

    render() {
        return (
            this.state.isLoading
                ? <AppLoader/>
                : <FormBuilder form={this.state.form} onSave={this.onSave} />
        )
    }
}

export default ChangePasswordForm;