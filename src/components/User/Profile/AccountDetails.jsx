import React from "react";
import ProfileForm from "./ProfileForm";
import {AppContext} from "../../App/AppContext";
import ChangePasswordForm from "./ChangePasswordForm";
import ChangeEmailForm from "./ChangeEmailForm";
import apiClient from "../../../infrastructure/Api/apiClient";
import {errorToast, successToast, toastFromQuery} from "../../../infrastructure/toast";
import {FaToggleOff, FaToggleOn} from "react-icons/all";

class AccountDetails extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            onChangePassword: false,
            onChangeEmail: false,
            onDeleteAccount: false,
            emailsNewsletter: false
        }
    }

    componentDidMount() {
        toastFromQuery();

        this.setState({emailsNewsletter: this.context.user.emailsNewsletter});
    }

    toggleDeleteAccount = () => {
        this.setState({
            onDeleteAccount: !this.state.onDeleteAccount,
            onChangePassword: false,
            onChangeEmail: false
        });
    }

    deleteAccount = () => {
        apiClient(this.context.token, this.context.deviceId).delete('/user').then(() => {
            document.cookie = `token=; Path=/; domain=.${process.env.REACT_APP_SITE_DOMAIN};Expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
            document.cookie = `token-expires-at=; Path=/; domain=.${process.env.REACT_APP_SITE_DOMAIN};Expires=Thu, 01 Jan 1970 00:00:01 GMT;`;
            document.cookie = `PHPSESSID=; Path=/; domain=${process.env.REACT_APP_SITE_DOMAIN};Expires=Thu, 01 Jan 1970 00:00:01 GMT;`;

            window.location.reload();
        });
    }

    toggleChangePassword = () => {
        this.setState({
            onChangePassword: !this.state.onChangePassword,
            onDeleteAccount: false,
            onChangeEmail: false
        })
    }

    toggleChangeEmail = () => {
        this.setState({
            onChangePassword: false,
            onDeleteAccount: false,
            onChangeEmail: !this.state.onChangeEmail
        })
    }

    toggleEmailsNewsletter = () => {
        const newStateValue = !this.state.emailsNewsletter;

        apiClient(this.context.token, this.context.deviceId).patch('/user', {email_subscription: newStateValue}).then(res => {
            this.setState({emailsNewsletter: newStateValue});

            newStateValue
                ? successToast(this.context.t('emails-newsletter-enabled'), 3000)
                : errorToast(this.context.t('emails-newsletter-disabled'), 4000);
        })
    }

    render() {
        const t = this.context.t;

        return (
            <div className="card card-small mb-4">
                <div className="card-header border-bottom">
                    <h6 className="m-0">{t('Account Details')}</h6>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item p-3">
                        <div className="row">
                            <div className="col mt-0">
                                <ProfileForm user={this.context.user}/>
                            </div>
                            <div className="col-12 mt-10">
                                <h6 className="mt-10 mb-0">{t('Danger zone')}</h6>
                                <div className="btn-group btn-group-toggle mt-2" data-toggle="buttons">
                                    <label className="btn btn-white">
                                        <input onClick={this.toggleChangePassword} type="radio" autoComplete="off"/>
                                        {t('Change password')}
                                    </label>
                                    <label className="btn btn-white">
                                        <input onClick={this.toggleChangeEmail} type="radio" autoComplete="off"/>
                                        {t('Change email')}
                                    </label>
                                    <label className="btn btn-danger">
                                        <input onClick={this.toggleDeleteAccount} type="radio" name="options"
                                               autoComplete="off"/> {t('Delete account')}
                                    </label>
                                </div>
                                {this.state.onChangePassword && <ChangePasswordForm onCancel={this.toggleChangePassword}/>}
                                {this.state.onChangeEmail && <ChangeEmailForm onCancel={this.toggleChangeEmail}/>}
                                {this.state.onDeleteAccount &&
                                    <>
                                        <h6 className="mb-0 mt-10">{t('Are you sure?')}</h6>
                                        <div className="btn-group btn-group-toggle mt-2" data-toggle="buttons">
                                            <label className="btn btn-danger">
                                                <input onClick={this.deleteAccount} type="radio" name="options"
                                                       id="option3"
                                                       autoComplete="off"/> {t('Yes')}
                                            </label>
                                            <label onClick={this.toggleDeleteAccount} className="btn btn-white">
                                                <input type="radio" autoComplete="off"/>
                                                {t('No')}
                                            </label>
                                        </div>
                                    </>
                                }
                            </div>
                            <div className="col-12 mt-10">
                                <h6 className="mt-10 mb-0">{t('Notifications')}</h6>
                                <div className="btn-group btn-group-toggle mt-2" data-toggle="buttons">
                                    <label className={`btn btn-${this.state.emailsNewsletter ? 'success' : 'danger'}`}>
                                        <input onClick={this.toggleEmailsNewsletter} type="radio" autoComplete="off"/>
                                        <span>{t('emails-newsletter')}</span>
                                        <span className={'ml-05'}>
                                            {this.state.emailsNewsletter ? <FaToggleOn/> : <FaToggleOff/>}
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
}

export default AccountDetails;