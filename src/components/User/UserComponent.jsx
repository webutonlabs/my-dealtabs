import React from "react";
import MainContent from "../../elements/MainContent";
import PageTitle from "../../elements/PageTitle";
import {AppContext} from "../App/AppContext";
import {Constants, Dispatcher, Store} from "../../flux";
import MembershipCard from "./Membership/MembershipCard";
import AccountDetails from "./Profile/AccountDetails";
import AppLoader from "../../elements/Loader/AppLoader";

class UserComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {isLoading: false}
    }

    componentDidMount() {
        document.title = this.context.t('title-my-dealtabs-user');

        if (Store.getMenuState() === true) {
            Dispatcher.dispatch({actionType: Constants.TOGGLE_SIDEBAR});
        }
    }

    render() {
        const t = this.context.t;

        return (
            this.state.isLoading ? <AppLoader/> :
                <>
                    <MainContent>
                        <PageTitle title={t('User profile')} subtitle={t('Overview')}/>
                        <div className="row mb-3">
                            <MembershipCard />
                            <div className="col-lg-8">
                                <AccountDetails />
                            </div>
                        </div>
                    </MainContent>
                </>
        )
    }
}

export default UserComponent;