import React from "react";
import {AppContext} from "../App/AppContext";

class LogoutComponent extends React.Component {
    static contextType = AppContext;

    componentDidMount() {
        window.location.href = `${process.env.REACT_APP_SITE_HOST}/logout`;
    }

    render() {
        return <h1>{this.context.t('Logging out...')}</h1>
    }
}

export default LogoutComponent;