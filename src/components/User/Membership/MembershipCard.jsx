import React from "react";
import {AppContext} from "../../App/AppContext";
import RestrictionList from "./RestrictionList";
import {isUserPremium} from "../../../infrastructure/helpers";
import {default as dateFormat} from "dateformat";

class MembershipCard extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        let expiresDate = null;
        if (isUserPremium(this.context.user)) {
            expiresDate = new Date(this.context.user.membership.subscriptionExpires);
        }

        return (
            <div className="col-lg-4">
                <div className="card card-small mb-4 pt-3">
                    <div className="card-header border-bottom text-center">
                        <h5>{this.context.user.name} {this.context.user.surname}</h5>
                        <h6>{t('Membership')}: {isUserPremium(this.context.user) ? t('Premium') : t('Basic')}</h6>
                        {(isUserPremium(this.context.user) && expiresDate !== null) &&
                        <h6>{t('Expires')}: {dateFormat(expiresDate, 'mmmm dd, yyyy HH:MM')}</h6>
                        }
                    </div>
                    <RestrictionList />
                </div>
            </div>
        )
    }
}

export default MembershipCard;