import React from "react";
import {AppContext} from "../../App/AppContext";
import AppLoader from "../../../elements/Loader/AppLoader";
import SmallListElement from "../../../elements/List/SmallListElement";
import SmallList from "../../../elements/List/SmallList";
import {MembershipRestrictions} from "../../../infrastructure/Membership/MembershipRestrictions";
import apiClient from "../../../infrastructure/Api/apiClient";

class RestrictionList extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            restrictions: new MembershipRestrictions([])
        }
    }

    componentDidMount() {
        apiClient(this.context.token, this.context.deviceId).get('/parameters/memberships').then(res => {
            this.setState({
                restrictions: new MembershipRestrictions(res.data).forUser(this.context.user),
                isLoading: false
            });
        });
    }

    render() {
        const t = this.context.t;
        const restrictions = this.state.restrictions;

        return (
            this.state.isLoading ? <AppLoader /> :
                <SmallList>
                    <SmallListElement
                        name={t('Session limit')}
                        value={typeof restrictions.sessionLimit() === 'number' ? restrictions.sessionLimit() : t('Unlimited')}
                    />
                    <SmallListElement
                        name={t('Windows per session limit')}
                        value={typeof restrictions.windowsLimit() === 'number' ? restrictions.windowsLimit() : t('Unlimited')}
                    />
                    <SmallListElement
                        name={t('Tabs per window limit')}
                        value={typeof restrictions.tabsLimit() === 'number' ? restrictions.tabsLimit() : t('Unlimited')}
                    />
                    <SmallListElement
                        name={t('quick-tabs-limit')}
                        value={typeof restrictions.quickTabsLimit() === 'number' ? restrictions.quickTabsLimit() : t('Unlimited')}
                    />
                    <SmallListElement
                        name={t('Device sync')}
                        value={restrictions.deviceSync() ? t('Enabled') : t('Disabled')}
                    />
                    <SmallListElement
                        name={t('collaboration')}
                        value={restrictions.collaboration() ? t('Enabled') : t('Partial')}
                    />
                    <SmallListElement
                        name={t('live-sync')}
                        value={restrictions.liveSync() === false ? t('Disabled') : t('Enabled')}
                    />
                </SmallList>
        )
    }
}

export default RestrictionList;