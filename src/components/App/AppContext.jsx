import React, {Component, createContext} from 'react'
import apiClient from "../../infrastructure/Api/apiClient";

export const AppContext = createContext(undefined)

class AppContextProvider extends Component {
    state = {
        isLoading: true,
        isAuthenticated: false,
        deviceId: null,
        token: null,
        user: {
            id: '',
            name: '',
            surname: '',
            email: '',
            isEmailConfirmed: false,
            locale: 'en',
            membership: {
                membershipType: false,
                subscriptionType: false,
                subscriptionExpires: false,
                isAllowedTrial: null,
                isCancelled: false
            },
        },
        translator: this.props.translator,
        t: this.props.t
    }

    componentDidMount() {
        this.setAccessToken();
        this.setDeviceId();

        if (null !== this.state.token && null !== this.state.deviceId) {
            apiClient(this.state.token, this.state.deviceId)
                .get('/user')
                .then((user) => {
                    this.setState({
                        isAuthenticated: true,
                        isLoading: false,
                        user: {
                            id: user.data.id,
                            name: user.data.name,
                            surname: user.data.surname,
                            email: user.data.email,
                            isEmailConfirmed: user.data['is_email_confirmed'],
                            locale: user.data['locale'],
                            emailsNewsletter: user.data['emails_newsletter'],
                            membership: {
                                membershipType: user.data.membership['membership_type'],
                                subscriptionType: user.data.membership['subscription_type'],
                                subscriptionExpires: user.data.membership['subscription_expires'],
                                isAllowedTrial: user.data.membership['allowed_trial'],
                                isCancelled: user.data.membership['cancelled_subscription']
                            },
                        }
                    })

                    this.state.translator.changeLanguage(user.data.locale);
                })
                .catch(() => {
                    setTimeout(() => {
                        this.setState({isLoading: false})
                    }, 2500)
                })
        } else {
            setTimeout(() => this.setState({isLoading: false}), 5000)
        }
    }

    setAccessToken() {
        const cookies = `; ${document.cookie}`;
        const parts = cookies.split(`; token=`);

        this.state.token = parts.length === 2 ? parts.pop().split(';').shift() : null;
    }

    setDeviceId() {
        const cookies = `; ${document.cookie}`;
        const parts = cookies.split(`; device-id=`);

        this.state.deviceId = parts.length === 2 ? parts.pop().split(';').shift() : null;
    }

    render() {
        return (
            <AppContext.Provider value={{...this.state}}>
                {this.props.children}
            </AppContext.Provider>
        )
    }
}

export default AppContextProvider
