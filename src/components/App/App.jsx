import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import AppRoutes from "./AppRoutes";

import "bootstrap/dist/css/bootstrap.min.css";
import "../../assets/styles/shards-dashboards.1.1.0.min.css";
import {useTranslation} from "react-i18next";
import AppContextProvider from "./AppContext";
import 'react-toastify/dist/ReactToastify.css';

function App() {
    const {t, i18n} = useTranslation();

    return (
        <AppContextProvider translator={i18n} t={t}>
            <Router>
                <AppRoutes/>
            </Router>
        </AppContextProvider>
    )
}

export default App;
