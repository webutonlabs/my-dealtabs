import React, {useContext} from 'react'
import {AppContext} from './AppContext'

import SessionsComponent from "../Sessions/SessionsComponent";
import {Redirect, Route, Switch} from "react-router-dom";
import DashboardLayout from "../../layouts/DashboardLayout";
import UserComponent from "../User/UserComponent";
import LogoutComponent from "../User/LogoutComponent";
import AppLoader from "../../elements/Loader/AppLoader";
import MembershipComponent from "../Membership/MembershipComponent";
import ArchivedTabsComponent from "../ArchivedTabs/ArchivedTabsComponent";
import LiveSynchronizationComponent from "../LiveSynchronization/LiveSynchronizationComponent";

const routes = [
    {path: '/sessions', component: SessionsComponent},
    {path: '/user', component: UserComponent},
    {path: '/membership', component: MembershipComponent},
    {path: '/archived-tabs', component: ArchivedTabsComponent},
    {path: '/live-sync', component: LiveSynchronizationComponent},
    {path: '/logout', component: LogoutComponent},
]

const AppRoutes = () => {
    const {isAuthenticated, isLoading} = useContext(AppContext);

    return isLoading ? (
        <AppLoader/>
    ) : isAuthenticated ? (
        <DashboardLayout>
            <Switch>
                {routes.map((route, index) => (
                    <Route key={index} path={route.path} exact={route.exact}>
                        <route.component/>
                    </Route>
                ))}
                <Redirect to='/sessions'/>
            </Switch>
        </DashboardLayout>
    ) : (() => {
        window.location.href = `${process.env.REACT_APP_SITE_HOST}/logout`;
        return null;
    })()
}

export default AppRoutes